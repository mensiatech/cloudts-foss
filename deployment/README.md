# Preparations

## Requisites
1. Install gcloud
2. Copy the template environment file `./deployment/google/google_cloud_env.tmpl.sh` to `./deployment/google/google_cloud_env_prod.sh` or `./deployment/google/google_cloud_env_dev.sh`
3. Set your environment by reviewing and running
   `source ./deployment/google/google_cloud_env_XXX.sh` where `XXX` is either
   `dev` or `prod`.

   The `prod` configuration used for Koala CloudTS is (the PROJECT ID is known after you create the project and the external IP is known when you create the IP address):

   ```shell
   # The name of the Google Cloud Platform project
   export PROJECT_ID="project-name"
   # The google compute region
   export REGION_NAME="europe-west1"
   export ZONE_NAME="europe-west1-b"
   # The bucket where the Data API will save its files
   export DATA_BUCKET_NAME="my-cloudts-data-bucket"
   # The bucket where the static files of Django will be served
   export STATIC_BUCKET_NAME="my-cloudts-static-bucket"
   # The bucket where the cluster stage files will be saved
   export DATAPROC_STAGE_BUCKET_NAME="my-cloudts-staging-bucket"
   # The name of the dataproc cluster for the Job API
   export CLUSTER_NAME="my-cloudts-cluster"
   # The number of workers on the dataproc cluster
   export SPARK_CLUSTER_NUM_WORKERS=2
   # The number of preemptible workers on the dataproc cluster
   export SPARK_CLUSTER_NUM_PREEMPTIBLE_WORKERS=0
   # The machine type for dataproc workers
   export SPARK_CLUSTER_WORKER_MACHINE="n1-standard-2"
   # The name of the service accounts that need bucket read/write permissions. This
   # is the client_email on the credentials.json file. You can put more than one if
   # they are separated by spaces
   export SERVICE_ACCOUNTS="default@project-name.iam.gserviceaccount.com"
   # The external IP address created for the CloudTS platform
   export CLOUDTS_EXTERNAL_IP="111.222.33.44"
   ```

4. Make sure that you are using the correct compute zone, where the cluster will
   be created.

  **Note:** for production we are using `europe-west1-b`, for development,
  `europe-west2-c`.

   To ensure this:

   ```shell
   gcloud config set project $PROJECT_ID
   gcloud config set compute/zone europe-west1-b # or europe-west2-c
   gcloud config get-value compute/zone
   ```

5. You need an external IP **if you have not reserved one yet.**.

    ```shell
    gcloud beta compute addresses create cloudts-static-ip \
        --description="CloudTS static IP address" \
         --region=europe-west1  # regional: using --global does not work. Check the correct region here
    # Get the IP from the result of this command:
    gcloud beta compute addresses list
    ```
   
   Important: At this moment you can fill the `CLOUDTS_EXTERNAL_IP` environment variable in the environment shell script and re-source the file!

## Environment variables

Take a moment to select the correct configuration on `deployment/google/google_cloud_env_XXX.sh`.

## Source code

1. For a real deployment, don't use a _dirty_ state of the git repository. Make
   sure that you have a clean, tagged commit of this repository.

## Buckets for data

To create the bucket where the data will be saved, use the script
`deployment/google/create_buckets.sh`.


## Persistent data disks

There must be a way to do the following automatically with Kubernetes, but I
don't know how (maybe
<http://blog.kubernetes.io/2017/03/dynamic-provisioning-and-storage-classes-kubernetes.html>?).

```shell
gcloud compute disks create --size=200GB cloudts-database-volume
```

```shell
gcloud compute instances create formatter-instance --machine-type "n1-standard-1" \
    --disk "name=cloudts-database-volume,device-name=cloudts-database-volume,mode=rw,boot=no" \
    --image "ubuntu-1604-xenial-v20170811" --image-project "ubuntu-os-cloud" \
    --boot-disk-size "10" --boot-disk-type "pd-standard" \
    --boot-disk-device-name "formatter-instance-boot-disk"
# Ignore the warning concerning the disk size: this concerns the boot disk 
# and this instance is just a temporary machine

# Now connect to the instance through ssh to format these disks:
gcloud compute ssh formatter-instance

# NOTE: The following commands (the ones prepended with $)
# should be executed in the instance.

# Find out where the disks are attached
$ sudo lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdb      8:16   0  200G  0 disk
sda      8:0    0   10G  0 disk
`-sda1   8:1    0   10G  0 part /

# Then do this command to format each disk:
# sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/[DEVICE_ID]
# In this example:
$ sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb

# Logout, this instance will no longer be needed
$ logout

# Finally, delete this instance. This may warn that the disks will be deleted, but this
# can be safely ignored, because this only applies to the boot disk, not the disk
# that we created for CloudTS
gcloud compute instances delete formatter-instance
```


## Build and push container images

1. Create and push the images with the following script. In this example, I have
   set the `CLOUDTS_VERSION` environment variable:

   ```shell
   export CLOUDTS_VERSION=x.y.z
   python deployment/create_images.py \
       --registry gcr.io \
       --version-tag $CLOUDTS_VERSION \
       --latest \
       --project-id $PROJECT_ID
   ```

## Cluster preparations

We need to create a cluster where the CloudTS application will run. Note: this
is not the Job API cluster.

For this you will need to enable the `Kubernetes Engine API` in the Google Cloud
Platform control panel.

1. Create a cluster on Google. This is the kubernetes cluster that runs _pods_
   for each service needed by CloudTS. That is: a Django web server, a Postgres
   database, a RabbitMQ queue, some workers and a HTTP proxy. Since there are
   4-5 services, we will create a cluster with 5 nodes.

    ```shell
    gcloud container clusters create cloudts-service-cluster --num-nodes=5
    ```

2. Create database deployment and service. First, create a secret for the
   username and password for the Postgres server. Note this down.

    ```shell
    kubectl create secret generic db-secrets \
        --from-literal=username=postgres \
        --from-literal=password=some_password
    kubectl describe secrets/db-secrets  # to check that the secrets were created
    ```

    Generate all deployment scripts, these are all templated as they require to
    know the CloudTS Project ID. Generate the script instances using:

    ```shell
    cd deployment
    ./instantiate_templates.sh google/google_cloud_env_XXX.sh
    ```

    The generated files will be in `deployment/kubernetes/instances`. 

    Verify that the `deployment/kubernetes/instances/db-deployment.yaml` file has the
    correct tag on the `spec/template/spec/containers[0]/image` entry. 
    You can see the tags in the Container Registry on the GCP. Also
    verify that the correct database volume has been configured on the
    `spec/template/spec/volumes[0]/gcePersistentDisk` entry.

    Then, create the service and deployment:

    ```shell
    kubectl create -f deployment/kubernetes/instances/db-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod db
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume db-1234:
    kubectl logs db-1234

    kubectl create -f deployment/kubernetes/db-service.yaml
    kubectl get services  # check the service was created
    ```
    
3. Verify that the `deployment/kubernetes/instances/rabbitmq-deployment.yaml` file has the
   correct tag on the `spec/template/spec/containers[0]/image` entry.

   Then, create the rabbitmq deployment and service:

    ```shell
    kubectl create -f deployment/kubernetes/instances/rabbitmq-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod rabbitmq
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume rabbitmq-1234:
    kubectl logs rabbitmq-1234

    kubectl create -f deployment/kubernetes/rabbitmq-service.yaml
    kubectl get services  # check the service was created
    ```
    
4. Create secrets and ensure that `web-deployment.yml` is using them as a volume

    ```shell
    kubectl create secret generic web-secrets \
        --from-file=./conf/secret.txt \
        --from-file=./conf/credentials.json \
        --from-file=./conf/admin.json
    kubectl describe secrets/web-secrets  # to check that the secrets were created
    ```


5. Verify that the `deployment/kubernetes/instances/web-deployment.yaml` file has the
   correct tag on the `spec/template/spec/containers[0]/image` entry.

   Verify that the environment variables in
   `spec/template/spec/containers[0]/env` has the correct values according to
   the `google_cloud_env_XXX.sh`.


   Then, create the web deployment and service:

    ```shell
    kubectl create -f deployment/kubernetes/instances/web-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod web
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume web-1234:
    kubectl logs web-1234

    kubectl create -f deployment/kubernetes/web-service.yaml
    kubectl get services  # check the service was created
    ```

6. Verify that the `deployment/kubernetes/worker-deployment.yaml` file has the
   correct tag on the `spec/template/spec/containers[0]/image` entry.

   Verify that the environment variables in
   `spec/template/spec/containers[0]/env` has the correct values according to
   the `google_cloud_env.sh`.
   
   Then, create the worker deployment. There is no service associated to this one.

    ```shell
    kubectl create -f deployment/kubernetes/instances/worker-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod worker
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume worker-1234:
    kubectl logs worker-1234
    ```
    
7. Verify that the `deployment/kubernetes/instances/beat-deployment.yaml` file has the
   correct tag on the `spec/template/spec/containers[0]/image` entry.

   Verify that the environment variables in
   `spec/template/spec/containers[0]/env` has the correct values according to
   the `google_cloud_env.sh`.
   
   Then, create the beat deployment. There is no service associated to this one.

    ```shell
    kubectl create -f deployment/kubernetes/instances/beat-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod beat
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume beat-1234:
    kubectl logs beat-1234
    ```

8. Create SSL secrets and ensure that nginx is using them as a volume:

    ```shell
    kubectl create secret generic ssl-secrets \
        --from-file=./docker/nginx/mysite.key \
        --from-file=./docker/nginx/mysite.crt

    kubectl describe secrets/ssl-secrets  # to check that the secrets were created
    ```

9. Verify that the `deployment/kubernetes/instances/nginx-deployment.yaml` file has the
   correct tag on the `spec/template/spec/containers[0]/image` entry.

   Make sure that the IP address that you reserved is set on the
   `deployment/kubernetes/instances/nginx-service.yaml` file, under the `loadBalancerIP` entry.

   Then, create the nginx deployment and service.

    ```shell
    kubectl create -f deployment/kubernetes/instances/nginx-deployment.yaml
    kubectl get pods  # to check the deployment was created; wait until it is Running
    # For the impatient, or if this is taking too long because there is a problem, 
    # examine the creation logs with:
    kubectl describe pod nginx
    # Or examine the logs, which needs the pod name (from kubectl get pods), 
    # let's assume nignx-1234:
    kubectl logs nginx-1234

    kubectl create -f deployment/kubernetes/instances/nginx-service.yaml
    kubectl get endpoints nginx  # check the load balancer matched nginx pods
    kubectl get services  # check the service was created. In particular, wait until
                          # the EXTERNAL-IP is assigned for the nginx service. This
                          # can take a few minutes.
    # More information if this is taking too long
    kubectl describe service nginx
    # Sometimes the forwarding-rules already use the IP so check the existing with:
    gcloud compute forwarding-rules list  # check the load balancer is working
    # and maybe delete the offending one. But make sure that the describe service nginx
    # suggests that there is a conflict
    ```

10. Check that your deployments are running the correct image versions:

    ```shell
    kubectl get pod -o=json | jq -r '.items[] | .metadata.name, .status.containerStatuses[].imageID'
    ```
    
    Or, if you do not have `jq` installed:

    ```shell
    kubectl describe pod  | grep Image\:
    ```



# Updates

Updates can be deployed with the following command. Remember to change the image
versions on each of the `deployment/kubernetes/[SERVICE]-deployment.yaml` files.

```shell
kubectl replace -f deployment/kubernetes/[SERVICE]-deployment.yaml
kubectl get pods  # to check the deployment was created; wait until it is Running
```

There is a way of doing rolling updates. I am not entirely sure the correct way
of doing it but the following seems to work:

First, use the `deployment/create_images.py` to create the new docker
images. Remember to use a new `--version-tag`. For example, if you want to
update the `web` deployment, first create its new image:

```shell
python deployment/create_images.py \
       --registry gcr.io \
       --version-tag X.Y.Z \
       --latest \
       --project-id $PROJECT_ID \
     --include web
```

Then roll the update with: (there is a `--record` option that I saw in the
example and I am not sure if this is useful, perhaps it is to rollback to the
previous version)

```shell
kubectl set image deployment web web="gcr.io/$PROJECT_ID/cloudts/web:X.Y.Z"
```

You will see how kubernetes brings the service down:

```shell
kubectl get pods
NAME                     READY     STATUS        RESTARTS   AGE
web-554743375-qkjcm      1/1       Terminating   0          8m
```

And then it will be started automatically with the new image:
```shell
kubectl get pods
NAME                     READY     STATUS    RESTARTS   AGE
web-2861698082-53j4s     1/1       Running   0          1m
```



# Cleaning up

If you want to temporarily disable the cluster, scale it down to zero. This
avoids billing on this cluster. (These commands assume that you have the
environment variables set).

```shell
gcloud container clusters resize ${CLUSTER_NAME} --size=0
```

Or you can delete it completely:

```shell
gcloud container clusters delete ${CLUSTER_NAME}
```

Also you might consider deleting the volumes, but **be careful**: if you did not
backup the data, you will lose it!

```shell
gcloud compute disks delete cloudts-database-volume
```

Remember to remove the forwarding rule that associated a load balancer to the
external IP:

```shell
gcloud compute forwarding-rules list
gcloud compute forwarding-rules delete [RULE_ID] --region [REGION_NAME]
```

The IP also should be deleted if it will not be used anymore, otherwise this
incurs on pricing:

```shell
gcloud beta compute addresses delete cloudts-static-ip --region [REGION_NAME]
```

# Job API cluster

To create and push the images that are used by the workers, use the script
`deployment/google/prepare_interpreters.sh`. Edit as necessary when new versions
of the interpreters are available. This script must be launched from the cloudts
root directory.

Once all images are created correctly, run.

```shell
deployment/google/prepare_interpreters.sh prod --upload
```

If you are adding new interpreters, remember to update the
`deployment/google/dataproc_init_actions.sh`. This is not automatic yet.

To create a cluster on Google Cloud Dataproc, use the
`deployment/google/create_cluster.sh` script. You can delete the same cluster
with the `deployment/google/delete_cluster.sh` script.

# Static files

1. Use docker-compose to obtain a Docker image with the web container

    ```shell
    docker-compose build
    ```

2. Create a volume to collect the static files

    ```shell
    docker volume create --label static-web-volume
    ```

3. Run the web container and collect static files in this volume

    ```shell
    docker-compose run --entrypoint "python manage.py collectstatic --no-input --clear" \
        -v static-web-volume:/var/www \
        -v `pwd`/conf:/code/conf \
        web
    docker-compose stop
    ```

4. Use the gcloud container to upload the contents of the static files volume to
   a Google bucket

    ```shell
    docker run -it --rm \
        -v static-web-volume:/var/www \
        -v `pwd`/conf:/tmp/conf:ro \
        --env GOOGLE_CREDENTIALS=/tmp/conf/credentials.json \
        cloudts/gcloud:latest \
        gsutil -m rsync -r -d /var/www/cloudts/static/ gs://${STATIC_BUCKET_NAME}/static
    ```

5. Delete the volume created to collect the static files

    ```shell
    docker volume rm static-web-volume
    ```

6. Login to the [administrative interface](https://your_ip/admin) 
   and ensure that the
   page is rendered correctly. That is, with images, css, colors, etc.

# Backups

In CloudTS, there are two important things to backup: the data files and the
metadata database. The data files are currently not backed up automatically,
we should think about making a cold bucket to do this.
The metadata files are in postgres database. 

This is the procedure to backup the postgres database. Assuming that the 
database is running on a pod named `db-pod-123`:

```shell
# First, connect to the pod and backup from there:
kubectl exec db-pod-123 -it bash
# The following command should be run from the resulting bash console:
mkdir -p /backups
pg_dump -U django_dbuser django_db | gzip > /backups/somedate_pgdump_cloudts.gz
# Calculate its checksum to ensure that you download it correctly later:
md5sum /backups/somedate_pgdump_cloudts.gz
# Disconnect from the pod
exit
# And now bring the backup to your local machine:
kubectl cp default/db-pod-123:/backups/somedate_pgdump_cloudts.gz \
           somedate_pgdump_cloudts.gz
# Verify the checksum of the local file
md5sum /backups/somedate_pgdump_cloudts.gz
```

# TODO

* ~~Username and password of the database service as secrets~~
* ~~Volumes: how to set size?~~
* ~~Volumes: are they persistent? Apparently not, data is lost when I resize the
  cluster!~~ [solved: it was a problem with the volume in the Dockerfile]
* ~~Volumes:~~ how to backup? R: It is possible to mount if volume is a persistent.
  volume created with gcloud
* ~~Static files deployment~~
* ~~Serve with gunicorn~~
* Consider dropping the DB service in favor of a Google SQL Postgres (seems
  non-trivial) or dive into the details of managing a database with replicas,
  and all that (beurk!).
* Backup the data files

# References

This tutorial: https://cloud.google.com/container-engine/docs/tutorials/hello-node

This one is also good: https://scotch.io/tutorials/google-cloud-platform-i-deploy-a-docker-app-to-google-container-engine-with-kubernetes
