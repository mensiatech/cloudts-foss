#!/bin/bash

# set -e  # stop on first error

if [ $# -ne 1 ]
then
    echo -ne "Error: Missing environment\nUsage: $0 environment\n\tWhere environment is prod or dev\n"
    exit 1
fi

ENV_ARG=$1
DIR=$(dirname "$0")

source ${DIR}/google_cloud_env_${ENV_ARG}.sh

command -v gsutil 2>&1 > /dev/null || {
    echo "gsutil is not installed, install it from https://cloud.google.com/sdk/"
}

echo "Creating static content bucket..."
if [[ -v STATIC_BUCKET_NAME ]]
then
    gsutil mb -c regional -l ${REGION_NAME} -p ${PROJECT_ID} gs://${STATIC_BUCKET_NAME}
else
    echo "No static content bucket created, set the STATIC_BUCKET_NAME variable"
fi

echo "Creating staging bucket for cluster..."
gsutil mb -c regional -l ${REGION_NAME} -p ${PROJECT_ID} gs://${DATAPROC_STAGE_BUCKET_NAME}

echo "Creating data bucket for data..."
gsutil mb -c regional -l ${REGION_NAME} -p ${PROJECT_ID} gs://${DATA_BUCKET_NAME}

echo "Setting permissions..."
if [[ -v STATIC_BUCKET_NAME ]]
then
    gsutil defacl ch -u AllUsers:R gs://${STATIC_BUCKET_NAME}
fi
# commented on the global data bucket because this should be done for each new user
#gsutil defacl ch -u AllUsers:R gs://${DATA_BUCKET_NAME}

for account in $(echo $SERVICE_ACCOUNTS | cut -d\  -f1-)
do
    if [[ -v STATIC_BUCKET_NAME ]]
    then
        gsutil acl ch -u ${account}:W gs://${STATIC_BUCKET_NAME}
    fi
    gsutil acl ch -u ${account}:W gs://${DATAPROC_STAGE_BUCKET_NAME}
    gsutil acl ch -u ${account}:W gs://${DATA_BUCKET_NAME}
done
