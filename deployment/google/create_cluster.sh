#!/bin/bash

# set -e: if any command fails, stop this script
set -e

if [ $# -ne 1 ]
then
    echo -ne "Error: Missing environment\nUsage: $0 environment\n\tWhere environment is prod or dev\n"
    exit 1
fi

ENV_ARG=$1
DIR=$(dirname "$0")

source ${DIR}/google_cloud_env_${ENV_ARG}.sh || exit

command -v gcloud 2>&1 > /dev/null || {
    echo "gcloud is not installed, install it from https://cloud.google.com/sdk/"
    exit 1
}

echo "Creating necessary Python wheels..."
TMP_WHEEL_DIR=$(mktemp -d)
TMP_WHEEL_BUCKET_DIR=gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/wheels/$(uuid -v4)
#pip wheel --no-deps --wheel ${TMP_WHEEL_DIR} dependencies/ncvt
pip wheel --no-deps --wheel ${TMP_WHEEL_DIR} -r ${DIR}/requirements-cluster-wheels.txt
# pip wheel --wheel ${TMP_WHEEL_DIR} -r ${DIR}/requirements-cluster.txt
gsutil -m cp ${TMP_WHEEL_DIR}/*.whl ${TMP_WHEEL_BUCKET_DIR}/
rm -r ${TMP_WHEEL_DIR}

echo "Uploading cluster initialization script and related files..."
gsutil cp ${DIR}/dataproc_init_actions.sh \
       gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/dataproc_init_actions.sh
gsutil cp ${DIR}/requirements-cluster.txt \
       gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/requirements.txt



echo "Creating cluster..."
# Change the number of workers and master/worker machine type as needed. The
# documentation on machine types is on
# https://cloud.google.com/compute/docs/machine-types or by running
# gcloud compute machine-types list
gcloud dataproc clusters create ${CLUSTER_NAME} \
       --project=${PROJECT_ID}\
       --initialization-action-timeout=10m \
       --initialization-actions=gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/dataproc_init_actions.sh \
       --metadata wheel_directory=${TMP_WHEEL_BUCKET_DIR} \
       --metadata python_requirements=gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/requirements.txt \
       --master-boot-disk-size=100GB \
       --master-machine-type=n1-standard-4 \
       --num-workers=${SPARK_CLUSTER_NUM_WORKERS} \
       --worker-boot-disk-size=100GB \
       --worker-machine-type=${SPARK_CLUSTER_WORKER_MACHINE} \
       --num-preemptible-workers=${SPARK_CLUSTER_NUM_PREEMPTIBLE_WORKERS} \
       --preemptible-worker-boot-disk-size=100GB \
       --region=global \
       --zone=${ZONE_NAME} \
       --properties yarn:yarn.resourcemanager.scheduler.monitor.enable=true \
       --properties yarn:yarn.log-aggregation-enable=true
#       --num-preemptible-workers=${SPARK_CLUSTER_NUM_WORKERS} \
#       --preemptible-worker-boot-disk-size=100GB \
#       --num-workers=2 \
#       --num-workers=${SPARK_CLUSTER_NUM_WORKERS} \
#TODO: the properties above need to be set all at once, comma separated.
# example: --properties 'spark:spark.master=spark://example.com,hdfs:dfs.hosts=/foo/bar/baz'

# Notes: Some additional options that may be interesting:

#`--properties spark:spark.executorEnv.PYTHONHASHSEED=0` to send some
#environment variables needed to tell Spark to use Python 3 instead of old
#Python. But this is no longer necessary, the initialization actions script
#handles this

#--properties yarn:yarn.log-aggregation-enable=true # maybe this fixes the logs?

# `--metadata key=value` to pass arguments to the initialization script.

# Don't erase the file from the google bucket, I think this is needed to scale
# up the cluster
# echo "Cleaning up..."
# gsutil rm gs://${DATAPROC_STAGE_BUCKET_NAME}/initialization-actions/dataproc_init_actions.sh
