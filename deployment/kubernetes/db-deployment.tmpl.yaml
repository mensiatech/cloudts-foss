# Documentation of these yaml, their schema and what everything means is
# available on https://kubernetes.io/docs/reference/
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: db
spec:
  # replicas: number of desired pods
  # for the db deployment, we will use only one at the moment since we have not
  # figured out how to use a persistent disk simultaneously among several pods
  replicas: 1
  # strategy: how new pods replace old ones.
  # here we use recreate (kill existing pods before creating new ones) due to
  # the persistent disk
  strategy:
    type: Recreate
  # template: the base configuration for creating pods
  template:
    # metadata: labels to help find/query/view applications and services
    metadata:
      labels:
        app: db
        tier: backend
    # spec: desired behavior of the deployment
    spec:
      containers:
        - name: db
          image: gcr.io/${PROJECT_ID}/cloudts/db:${CLOUDTS_VERSION}
          imagePullPolicy: Always # Always pull the image, do not use any cache
          ports:
            - containerPort: 5432
          resources:
            requests:
              cpu: 500m
          #     memory: 100Mi
          # volumeMounts: volumes mounted on each pod
          volumeMounts:
            - name: db-data-volume
              mountPath: /srv/data
          # env: environment variables set for each pod
          env:
            # for the database contents, as documented in the postgres:latest
            # Dockerfile configuration in hub.docker.com, it is recommended to
            # change the PGDATA to a subdirectory. I tried not using a
            # subdirectory before (/var/lib/postgresql/data or
            # /var/lib/postgresql/data/pgdata). The first case doesn't work
            # because there is a volume in the Dockerfile which creates a silent
            # conflict and then the persistent data is lost. The second case
            # doesn't work because postgres complains that this is a mount
            # point.
            - name: PGDATA
              value: /srv/data/pgdata
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: db-secrets
                  key: username
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: db-secrets
                  key: password
      # restartPolicy: what to do when the pod dies
      restartPolicy: Always
      # volumes: list of volumes that can be mounted by containers
      volumes:
        - name: db-data-volume
          gcePersistentDisk:
            # This volume must already exist in Google Cloud (see deployment.md)
            pdName: cloudts-database-volume
            fsType: ext4
