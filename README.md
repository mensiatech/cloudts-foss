# README #

CloudTS is a web application that permits the management of data and metadata
through a REST API. It will also permit the execution of jobs on this data.

Current version: 0.2.0

Dependencies: Python 3.5+ and the packages listed in the `requirements.txt`.

OS: This has only been tested on Linux.

# Preparations #

First, you will need to clone or download this repository.

Next, you will need to obtain or generate several configuration files that are
sensitive and therefore not included in the git repository. Here is how to
generate them:

### Django secret file

Django secret string, used internally: generate one as a random string of 50
chars. For example:

```python
from django.utils.crypto import get_random_string
secret = get_random_string(length=50)
with open('conf/secret.txt', 'w') as f:
    f.write(secret)
```

This generates a file on `conf/secret.txt`

Note that this uses the `django` package. You may want to install the
requirements using `pip install -r requirements.txt`.

### Django superuser file

The administrator of the Django app is created automatically when you have a
file `conf/admin.json` with the following contents, so create such file and fill
with a password:

```json
{
    "username": "admin",
    "password": "some_secret_password",
    "email": "cloudts-admin@example.com"
}
```

### Google cloud credentials

Google Cloud credentials are used for the Google backend (CloudTS Job API). Get
one from your Google Cloud administrator. This is found on
the [Google Cloud Platform](https://console.cloud.google.com) on the API Manager
tab: Credentials, then Create credentials, service account key (Compute Engine
default service account, in JSON format).
  
Once you obtain this file, save it on `conf/credentials.json`

### SSL certificates

SSL certificates. This is particular to the `nginx` proxy server, so put them in
the `docker/nginx` directory. At the moment, we are using development
certificates, which are self-signed. This can be generated as follows:
  
```shell
cd docker/nginx
openssl req -x509 -newkey rsa:4096 -keyout mysite.key -out mysite.crt -days 365 -nodes
# You'll have to fill some elements shown in the output:
# Country Name (2 letter code) [AU]:FR
# State or Province Name (full name) [Some-State]:
# Locality Name (eg, city) []:Paris
# Organization Name (eg, company) [Internet Widgits Pty Ltd]:Organization Name
# Organizational Unit Name (eg, section) []:R&D
# Common Name (e.g. server FQDN or YOUR name) []:Your name
# Email Address []:some_address@example.com
```

At the moment, the `nginx` is configured to use the files named
`docker/nginx/mysite.crt` and `docker/nginx/mysite.key`. In the future, we will
need to obtain a proper certificate, like the ones created in <https://letsencrypt.org>.


### SSH keys

Generate a SSH public key with no passphrase with the following command:

```shell
ssh-keygen -f conf/bitbucket

Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
```

Add the public key `conf/bitbucket.pub` to your bitbucket configuration. The
docker build process will use it for getting the code of Mensia
repositories. However, by using multi-stage builds, the key will not be shared
and sent around.


# Setup #

The easiest way to run CloudTS is to run it as different micro-services
connected together with `docker-compose`. For this, you will need to install the
Docker Engine and then `docker-compose`. Follow
the
[`docker-compose` installation instructions](https://docs.docker.com/compose/install) to
install both of these tools.

## Setup using `docker-compose` ##

Running CloudTS with `docker-compose` is straightforward. First, build the Docker services:

```shell
docker-compose build
```

Then, run CloudTS with:

```shell
docker-compose up
```

The first time you run CloudTS, you will need to setup an administrator
account. To create one, ensure that the services are running and run the
following command:

```shell
docker-compose exec web python manage.py createsuperuser
```

This will prompt for a username and password. Use these credentials to login on
the administration interface at <https://localhost/admin> to create some users.

You can stop the server with:

```shell
docker-compose stop
```

And remove it with:
```shell
docker-compose down
```

**WARNING:** Using the `down` command will destroy any volumes, so you will lose
whatever was saved on the database used by Django (users, workspaces, jobs).


## Setup without `docker-compose` ##

You can also run CloudTS without `docker-compose`, which is still interesting
for debugging purposes. However, making everything work is harder because CloudTS is
designed as several interconnected microservices. 

First, to run CloudTS on this development environment, it is recommended that
you create a Python virtual environment (for more info see
the
[Python guidelines](https://jira.mensiatech.com/confluence/display/RES/Python+guidelines).

Once your virtual environment is created and activated, install the
dependencies:

```shell
pip install -r requirements.txt
pip install -r requirements-test.txt  # If you want to run the unit tests
pip install -r requirements-dev.txt   # Optional, some useful development tools
```

Now you need to decide how you Django is going to use a database. The easiest
way would be to replace the `default` database with an SQLite database on the
Django configuration file `cloudts/settings.py`. This is easy and simple enough
for tests. Here is an example configuration for this case:

```python
DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.sqlite3',
       'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
   }
}

```

Also, for development purposes only, you will need to comment the variables in
the `cloudts/settings.py` file under the *SSL related* section. Otherwise,
Django will redirect all requests to https, which are handled by another
microservice.

If you are using the Google backend for the Data API, you will need to set some
environment variables. Use the `deployment/google/google_cloud_env.sh` script.

Prepare the Django application by creating the database tables:

```shell
python manage.py makemigrations
python manage.py migrate
```

Create an administrator user:

```shell
python manage.py createsuperuser
```

Run the server. In this case, it will run in the 8000 port:

```shell
python manage.py runserver 0.0.0.0:8000
```

You can now test if the web application is running by visiting
http://localhost:8000/api/v1

An administrative interface is available on http://localhost:8000/admin where
you can create normal users.


# Unit tests #

Update on February 2018: all unit tests are most likely out of date; they tested
the old Data API backend code, which is no longer used. We need help on this!

Unit tests are written in Python using the `unittest` library. Since this is a
Django application, the unit testing structure follows the guidelines of
Django. To run the tests, use the following command:

```shell
python manage.py test
```

Some tests only work when you are a super user (root), so these should be tested
under a Docker container. To run any command on the Docker container, make sure
you have your containers up and running, then prepend a command as the following
example:

```shell
docker-compose exec web python manage.py test
```

# Django settings #

To be throughly described in the future. At the moment, you can browse the
`cloudts/settings.py` Django configuration file.

To-do list for this documentation:

* Database configuration
* New apps configuration
* Backend configuration

# Examples #

You can use the [Insomnia](https://insomnia.rest/) application to test the
endpoints of CloudTS APIs. There is a prepared configuration file in
`insomnia/CloudTS_APIs.json`.

# Deployment #

See the `deployment/README.md` for details.


### Contribution guidelines ###

* Writing tests
  * The data API and its backends are tested with unit tests written in the
    `cloudts/api/data/tests` directory. But most likely out of date.
  * We need help on expanding these tests.
* Code review
  * PR are welcome, use the bitbucket PR system.
* Other guidelines
  * The standard 
    [Python guidelines](https://jira.mensiatech.com/confluence/display/RES/Python+guidelines).

# Authors #

See `AUTHORS.md` for details.

# License #

CloudTS is [BSD 3-Clause licensed](./LICENSE)
