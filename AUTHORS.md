Authors of CloudTS, organized by their associations:

# Mensia Technologies

* David Ojeda [2016-2018]
* Louis Mayaud [2016-2018]
* Jozef Legeny [2018]
* Adrian Ahne [2017]

# Without association

* David Ojeda [2018-...]
