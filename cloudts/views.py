# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import StaticHTMLRenderer

from cloudts import __version__

from cloudts.celery import debug_task

class LogoutView(APIView):

    def post(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class OkView(APIView):
    permission_classes = (permissions.AllowAny,)
    renderer_classes = (StaticHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        data = """
        <html>
        <body>
        <h1>CloudTS</h1> 
        Welcome! <br/>
        
        You might want to use the 
        <a href="https://bitbucket.org/mensiatech/cloudts-client">cloudts-client</a> 
        Python library to use the ReST APIs of CloudTS. 
        </body>
        </html>
        """.strip()
        return Response(data=data, status=status.HTTP_200_OK)


class VersionView(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self, request, *args, **kwargs):
        return Response(data={'version': __version__},
                        status=status.HTTP_200_OK)
