# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
import json
import os

import django


def main():

    django.setup()
    from django.contrib.auth import get_user_model

    UserModel = get_user_model()
    print('User model is', UserModel)
    admins = UserModel.objects.filter(is_superuser=True)
    if not admins:
        # No admin exists, load it from a file
        print('No admin users exist')
        with open('/code/conf/admin.json', 'r') as f:
            admin_info = json.load(f)
        UserModel.objects.create_superuser(admin_info['username'],
                                           admin_info['email'],
                                           admin_info['password'])
    else:
        print('There are already some super users:', admins)

if __name__ == '__main__':
    main()
