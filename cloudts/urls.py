# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

"""cloudts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.authtoken import views as auth_views
# from rest_framework_swagger.views import get_swagger_view  # Enable when fixed

from cloudts.views import OkView, VersionView, LogoutView


urlpatterns = [
    # Health check
    url(r'^$', OkView.as_view()),
    url(r'^version/', VersionView.as_view()),
    url(r'^ht/', include('health_check.urls')),
    # Administrative interface
    url(r'^admin/', admin.site.urls),
    # Authentication
    url(r'^v1/auth/token/', auth_views.ObtainAuthToken.as_view()),
    url(r'^v1/auth/logout/', LogoutView.as_view()),
    # Data API
    url(r'^v1/data/', include('cloudts.api.data.urls')),
    # Job API
    url(r'^v1/job/', include('cloudts.api.job.urls')),
    # Swagger (OpenAPI) documentation: Does not work but it will one day!
    # url(r'^v1/docs/$', get_swagger_view(title='CloudTS API (version 1)')),
]
