# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" Celery application definition """
import os
import logging

from celery import Celery


logger = logging.getLogger(__name__)

# set the default Django settings module for the 'celery' program.
# Q: is this really needed?
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cloudts.settings')

app = Celery('cloudts', loader='cloudts.loaders.CustomLoader')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
app.config_from_object('cloudts.celeryconfig')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    logger.info('%s debug_task is running', self)
