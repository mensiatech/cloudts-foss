# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from .base import *

# In prod, the SECRET_KEY can only come from a file in conf/ that is set through
# a kubernetes secret
SECRET_KEY = open(os.path.join(BASE_DIR, 'conf', 'secret.txt')).read().strip()

# Never debug on production
DEBUG = False

# Accept from any, there are some health checks and I don't know where they come
# from
ALLOWED_HOSTS = ['*']

# Static content comes from a bucket
STATIC_URL = 'https://storage.googleapis.com/cloudts-beta-web/static/'


# CloudTS specific settings
CLOUDTS_CONFIG = {
    'data_backend': {
        ## Google and postgres backend
        'ENGINE': 'cloudts.api.data.backends.google_pg.GoogleJSONBackend',
        'OPTIONS': {
            'bucket_name': os.getenv('DATA_BUCKET_NAME'),
            'json_credentials': os.getenv('GOOGLE_CREDENTIALS'),
            'region': os.getenv('GOOGLE_REGION'),
            'prefix': 'cloudts-beta-ws',
            'enforce_permissions': True,
            'readonly_user': 'readonly_user',
        },
    },
    'job_backend' : {
        ## local job backend.
        'engine': 'cloudts.api.job.backends.google.GoogleJobBackend',
        'options': {
            'project_name' : os.getenv('PROJECT_ID'),  # Name of the Google Cloud project
            'cluster_name' : os.getenv('CLUSTER_NAME'),  # Name of the Dataproc cluster for jobs
            'bucket_name' :  os.getenv('DATAPROC_STAGE_BUCKET_NAME'),  # Name of the staging bucket, used to send files (scripts, jsons) to the job cluster
            'basedir_cloud' : 'cluster-files',  # Base directory on the staging bucket
            #TODO: we should have a different set of credentials for NCVT; to
            #avoid that NCVT write directly into the global data bucket
            'json_credentials': os.getenv('GOOGLE_CREDENTIALS'),
        },
    },
}
