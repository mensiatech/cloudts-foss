# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from .base import *

# On dev: debug and allow from anywhere
DEBUG = True
ALLOWED_HOSTS = ['*']

# Static files are resolved to /static/
STATIC_URL = '/static/'
# And they are physically in this directory
STATIC_ROOT = '/var/www/cloudts/static'


# CloudTS specific settings
CLOUDTS_CONFIG = {
    'data_backend': {
        # ## Local data backend. Useful for development and tests
        # 'ENGINE': 'cloudts.api.data.backends.local.LocalBackend',
        # 'OPTIONS': {
        #     ## For a dockerized server:
        #     #'basedir': '/srv/cloudts/data',
        #     #'enforce_permissions': True,
        #     ## For a local server:
        #     'basedir': '/tmp/cloudts',
        #     'enforce_permissions': False,
        # },
        # ## Google data backend
        # 'ENGINE': 'cloudts.api.data.backends.google.GoogleBackend',
        # 'OPTIONS': {
        #     'bucket_name': 'cloudts-dev-data',
        #     'local_basedir': '/srv/cloudts',
        #     'prefix': 'cloudts-dev-ws',  # prefix name for all workspaces
        #     'json_credentials': os.path.join(BASE_DIR, 'conf', 'credentials.json'),
        #     'region': 'EUROPE-WEST2',
        #     'enforce_permissions': False,
        # },
        ## Google and postgres backend
        'ENGINE': 'cloudts.api.data.backends.google_pg.GoogleJSONBackend',
        'OPTIONS': {
            'bucket_name': 'cloudts-dev-data',
            'json_credentials': os.getenv('GOOGLE_CREDENTIALS',
                                          os.path.join(BASE_DIR, 'conf', 'credentials.json')),
            'region': 'europe-west2',
            'prefix': 'cloudts-dev-ws',
            'enforce_permissions': True,
            'readonly_user': 'readonly_user',
        },
    },
    'job_backend' : {
        ## local job backend.
        'engine': 'cloudts.api.job.backends.google.GoogleJobBackend',
        'options': {
            'project_name' : 'cloudts-176214',  # Name of the Google Cloud project
            'cluster_name' : 'cloudts-dev-job-cluster',  # Name of the Dataproc cluster for jobs
            'bucket_name' :  'cloudts-dev-cluster-stage',  # Name of the staging bucket, used to send files (scripts, jsons) to the job cluster
            'basedir_cloud' : 'cluster-files',  # Base directory on the staging bucket
            #TODO: we should have a different set of credentials for NCVT; to
            #avoid that NCVT write directly into the global data bucket
            'json_credentials' : os.path.join(BASE_DIR, 'conf', 'credentials.json'),
        },
    },
}


# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'cloudts': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'ncvt': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'root': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    },
}
