# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


from rest_framework_nested.routers import DefaultRouter, NestedDefaultRouter

from cloudts.api.job import views

urlpatterns = []

# A router creates URLs and associates them with the correct method of a
# viewset.
router = DefaultRouter()
router.include_format_suffixes = False

## Router configuration for jobs
# /jobs/
router.register(r'jobs',
                views.JobViewSet,
                base_name='job')
urlpatterns += router.urls

# Logs: commented; this still needs work
# # /jobs/<pk>/logs/
# logs_router = NestedDefaultRouter(router,
#                                   parent_prefix=r'jobs',
#                                   lookup='job')
# logs_router.include_format_suffixes = False
# logs_router.register(r'logs',
#                      views.LogViewSet,
#                      base_name='job-logs')
# urlpatterns += logs_router.urls



