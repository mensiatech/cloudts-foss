# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import argparse
import collections
import copy
import datetime
import functools
import io
import json
import logging
import os
import random
import subprocess
import sys
import tempfile
import tarfile
import urllib
from posixpath import join as urljoin

import frozendict
import pytz
from cloudts.client.google import RobustGoogleStorageClient
from retryable import retry
from google.cloud.storage import Client as GoogleClient
from cryptography.fernet import Fernet

import pyspark
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession, SQLContext


SECRET_KEY = b'3VM1cm_NHYzUbNNFGdlbzTg6w3TJsUcivZo6h4kmEHk='


class WorkerException(Exception):
    pass


class MasterException(Exception):
    pass


# LOGGING
# Notes (dojeda): logging does not work on the Spark workers. I initially
# thought that this was some serious problem/shortcoming from either Google or
# Apache. Due to frustration, and too much time trying get this working, I gave
# up. There are some alternatives, such as using the google libraries to log to
# their "Stackdriver logging". See this link
# https://cloud.google.com/logging/docs/integrate/python
#
# Another option is to have our transformations return a tuple, where the second
# element is the standard output, and the master gathers this. This is the
# implementation that follows
class WorkerLogger(logging.Logger):
    def __init__(self, name=None, level=None, context_info=None):
        super().__init__(name, level)
        self.stream = io.StringIO()
        self.stream_handler = logging.StreamHandler(self.stream)
        formatter = logging.Formatter('%(asctime)s - %(name)s - {extra} - %(levelname)s - %(message)s'
                                      .format(extra=context_info or 'no context info'))
        self.stream_handler.setFormatter(formatter)
        self.addHandler(self.stream_handler)

    def collect_log(self):
        self.stream_handler.flush()
        self.stream.seek(0)
        return self.stream.read()


def log_wrapper(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        import socket
        from pyspark import TaskContext
        ctx = TaskContext()
        logger = WorkerLogger('cloudts.worker.{}'.format(func.__name__),
                              level=logging.INFO,
                              context_info='Spark:[S-{0} P-{1} {2}]'
                              .format(ctx.stageId(),
                                      ctx.partitionId(),
                                      socket.gethostname()))
        kwargs['logger'] = logger
        try:
            result = func(*args, **kwargs)
        except:
            logger.warning('Worker finished with exception', exc_info=True)
            result = None
        return result, logger.collect_log()
    return wrapper


@retry(count=5)  # Retryable because it uses a metadata http service of GCE
def initialize_google_client():
    return RobustGoogleStorageClient()


@retry(count=5)
def upload_file(blob, filename):
    with open(filename, 'rb') as f:
        blob.upload_from_file(f)


@retry(count=5)
def upload_string(blob, contents):
    blob.upload_from_string(contents)


class BucketManager:

    def __init__(self, config, client, logger):
        self.logger = logger
        if len(set(config.values())) != len(config):
            logger.error('Some of the configuration values are repeated %s'
                         .format(config))
            raise WorkerException('Invalid configuration for buckets: '
                                  'repeated values')
        self.config = config
        self.logger.info('Config is %s', config)
        self.client = client
        self.buckets = {
            k: self.client.get_bucket(urllib.parse.urlparse(v).netloc)
            for k, v in config.items()
            if k.endswith('_dir')
        }
        self.dirs = {
            k: tempfile.TemporaryDirectory()
            for k in config
        }
        self.dirs['json_temp'] = tempfile.TemporaryDirectory()
        self.bind_config = {
            self.dirs['json_temp'].name: {
                'bind': '/mnt/json_temp',
                'mode': 'ro',
            }
        }
        for k, v in self.config.items():
            mode = 'rw'
            if k in ('data_dir', 'scripts_dir'):
                mode = 'ro'
            self.bind_config[self.dirs[k].name] = {
                'bind': os.path.join('/mnt', k),
                'mode': mode
            }
        self._msg()

    def _msg(self):
        msg = []
        for k, v in self.config.items():
            if not k.endswith('_dir'): continue
            msg.append('[{} = {}] bucket {} -> {}'.format(k, v,
                                                          self.buckets[k].name,
                                                          self.dirs[k].name))
        self.logger.info('Bucket manager\n%s', '\n'.join(msg))

    @property
    def docker_volumes(self):
        return self.bind_config

    def resolve(self, value, download=False, can_be_missing=False):
        if isinstance(value, str):
            return self.resolve_mapping({'key': value}, keys={'scripts_dir'},
                                        download=download,
                                        can_be_missing=can_be_missing)['key']
        else:
            return self.resolve_mapping(value, keys=None, download=download,
                                        can_be_missing=can_be_missing)

    def resolve_mapping(self, mapping, keys=None, download=False, can_be_missing=False):
        new_mapping = {}
        if mapping is None:
            return new_mapping
        keys = keys or set(self.config.keys())
        # A mapping key: value, where value is something in one of our config
        # directories
        for key, values in mapping.items():
            # Convert to list to manage all/each tokens the same way
            is_list = isinstance(values, list)
            if not is_list:
                values = [values]
            else:
                values = values[:]
            for i, v in enumerate(values):
                modified = False
                # Check if the value is inside any of the configuration files
                for conf, path in self.config.items():
                    if conf not in keys:
                        continue
                    if v.startswith(path):
                        # TODO: improve this ugly code: get the file path, then
                        # build from that
                        #file_path = v.replace(path.rstrip('/'), '', 1).lstrip('/')  # this was working, but not for scripts
                        file_path = urllib.parse.urlparse(v).path.strip('/')
                        self.logger.info('file_path of %s is %s', path, file_path)
                        temp_path = os.path.join(self.dirs[conf].name, file_path)
                        mount_path = os.path.join(self.bind_config[self.dirs[conf].name]['bind'], file_path)
                        # Create the directory structure
                        os.makedirs(os.path.split(temp_path)[0], exist_ok=True)
                        values[i] = mount_path
                        self.logger.info('REPLACED %s -> %s -> %s', v, temp_path, values[i])
                        if download:
                            #blob_path = urllib.parse.urlparse(v).path.lstrip('/')
                            bucket = self.buckets[conf]
                            blob = bucket.get_blob(file_path)
                            self.logger.info('Downloading from bucket gs://%s/%s', bucket.name, file_path)
                            if blob is None:
                                if not can_be_missing:
                                    raise WorkerException('Blob gs://{}/{} not found'.format(bucket.name, file_path))
                            else:
                                with open(temp_path, 'wb') as f:
                                    blob.download_to_file(f)
                        # stop the config loop, there should not be any repeated
                        # elements (TODO: improve the check in the constructor)
                        # THIS IS A PROBLEM FOR SCRIPTS
                        modified = True
                        break
                if not modified:
                    self.logger.warning('Value %s was not resolved', v)
            if is_list:
                new_mapping[key] = values
            else:
                new_mapping[key] = values[0]
        return new_mapping

    def is_stale(self, job):
        # This is mostly a rewrite/adaptation of ncvt.jobs.base.is_updated
        # but reworded to return True when it is stale, i.e. outdated.
        # (in ncvt it returns True when it's up to date, which is the opposite)
        #
        # === Special cases ===
        # Special case 1: forced through configuration file: does not apply to cloudts
        # Special case 2: forced through the json file: not implemented in ncvt or here
        # Special case 3: when the job has no outputs; it is impossible to know
        if not job.get('Outputs', {}):
            self.logger.info('Job has no outputs, considered stale')
            return True

        # Special case 4: metadata collection is always out of date: does not apply to
        # cloudts because this is not run on the Spark worker

        # === Regular case ===
        # determine latest time modification of inputs + scripts and compare
        # them to outputs
        latest_input = (datetime.datetime(datetime.MINYEAR, 1, 1, tzinfo=pytz.UTC), '')
        # Scripts are considered inputs
        if job['Interpreter'] not in ('NCVT', 'NeuroRTTool'):
            script_mtime = self.get_mtime(job['Name'])
            latest_input = max(latest_input, (script_mtime, job['Name']))
        if job['Interpreter'] == 'NeuroRT':
            # For NeuroRT, also check the conf file
            neurort_conf_file = os.path.splitext(job['Name'])[0] + '.conf'
            conf_mtime = self.get_mtime(neurort_conf_file, can_be_missing=True)
            latest_input = max(latest_input, (conf_mtime, neurort_conf_file))

        input_dict = job.get('Inputs', {})
        for input_name, input_files in input_dict.items():
            if not isinstance(input_files, list):
                input_files = [input_files]
            for fi in input_files:
                self.logger.info('Checking if input %s is stale: %s',
                                 input_name, fi)
                input_mtime = self.get_mtime(fi)
                if input_mtime is None:
                    self.logger.info('Input %s does not exist! Assuming stale')
                    return True
                latest_input = max(latest_input, (input_mtime, fi))

        earliest_output = (datetime.datetime(datetime.MAXYEAR, 12, 31,
                                             23, 59, 59, 999999, tzinfo=pytz.UTC),
                           '')
        output_dict = job.get('Outputs', {})
        for output_name, output_files in output_dict.items():
            if not isinstance(output_files, list):
                output_files = [output_files]
            for fo in output_files:
                self.logger.info('Checking if output %s is stale: %s',
                                 output_name, fo)
                output_mtime = self.get_mtime(fo, can_be_missing=True)
                earliest_output = min(earliest_output, (output_mtime, fo))

        if latest_input >= earliest_output:
            self.logger.info('Job is stale due to: %s >= %s',
                             latest_input[1], earliest_output[1])
        return latest_input >= earliest_output

    def get_mtime(self, url, can_be_missing=False):
        conf_keys = ('data_dir', 'scripts_dir', 'results_dir', 'data_temp_dir')
        for conf, path in self.config.items():
            if conf not in conf_keys:   continue
            if not url.startswith(path): continue
            file_path = urllib.parse.urlparse(url).path.strip('/')
            bucket = self.buckets[conf]
            blob = bucket.get_blob(file_path)
            self.logger.info('Check blob %s', blob)
            if blob is None:
                if can_be_missing:
                    self.logger.info('Blob %s does not exist but a missing file '
                                     'is accepted', file_path)
                    continue
                else:
                    self.logger.info('Blob %s does not exist and it cannot be '
                                     'missing! Assuming stale', file_path)
                    return None
            else:
                # blob.updated, the date of the latest change on the blob
                return blob.updated
        if can_be_missing:
            return datetime.datetime(datetime.MINYEAR, 1, 1, tzinfo=pytz.UTC)
        return None

    def upload(self, mapping):
        if mapping is None:
            return
        for key, values in mapping.items():
            # Convert to list to manage all/each tokens the same way
            is_list = isinstance(values, list)
            if not is_list:
                values = [values]
            else:
                values = values[:]
            for i, v in enumerate(values):
                self.logger.info('should upload %s', v)
                uploaded = False
                # Check if the value is inside any of the permitted directories
                for conf, path in self.config.items():
                    if conf not in ('results_dir', 'data_temp_dir'):
                        continue
                    if v.startswith(path):
                        # TODO: improve this ugly code: get the file path, then
                        # build from that
                        #file_path = v.replace(path.rstrip('/'), '', 1).lstrip('/')  # this was working, but not for scripts
                        file_path = urllib.parse.urlparse(v).path.strip('/')
                        self.logger.info('file_path is %s', file_path)
                        temp_path = os.path.join(self.dirs[conf].name, file_path)
                        bucket = self.buckets[conf]
                        # for root, dirs, files in os.walk(self.dirs[conf].name):
                        #     for _f in files:
                        #         self.logger.info('Existing file %s', _f)
                        self.logger.info('Uploading %s -> %s', temp_path, v)
                        # Use a retryable function
                        blob = bucket.blob(file_path)
                        upload_file(blob, temp_path)
                        # with open(temp_path, 'rb') as f:
                        #     blob.upload_from_file(f)
                        # Set the owner to this file
                        acl = blob.acl
                        self.logger.info('Setting permissions for %s', self.config['owner_email'])
                        acl.user(self.config['owner_email']).grant_owner()
                        acl.save()
                        #self.upload_to_cloudts(v)
                        # stop the loop over the permitted directories
                        uploaded = True
                        break
                if not uploaded:
                    self.logger.warning('Value %s was not uploaded', v)

    # This is no longer done on the spark worker, but on the celery worker, who
    # manages the metadata et al.
    # def upload_to_cloudts(self, file_url):
    #     # TODO: while we wait for RES-157, let's do the upload manually
    #     url = '{0}/v1/data/files/'.format(self.config['cloudts_url'])
    #     data = {
    #         'workspace': self.config['workspace_id'],
    #         'url': file_url,
    #     }
    #     response = requests.post(url, json=data, verify=False,
    #                              headers={'Authorization': 'Token {0}'.format(self.config['token'])})
    #     if response.status_code not in (200, 201):
    #         self.logger.error('Upload returned code {},  data {}'
    #                           .format(response.status_code, response.json()))
    #         raise WorkerException('Failed to upload file {}'.format(file_url))
    #     response_json = response.json()
    #
    #     return response_json['url']

    def upload_logs(self, filename, contents, status, container, extra=None, secret=None):
        secret = secret or {}
        # Extract standard output and standard error
        stdout = stderr = ''
        docker_details = 'No docker container executed'
        if container is not None:
            stdout = container.logs(stdout=True, stderr=False).decode()
            stderr = container.logs(stdout=False, stderr=True).decode()

            # Extract the docker details
            docker_details = json.dumps(container.attrs, indent=2)

        # Get the log bucket where the contents will be saved
        logs_bucket = self.buckets['logs_dir']
        logs_path = urllib.parse.urlparse(self.config['logs_dir']).path.strip('/')
        #json_filename = filename.replace(self.config['jobs_dir'], '', 1).strip('/')
        json_filename = os.path.basename(filename)
        if status is None:
            subdir = '_IGNORED'
        elif status:
            subdir = '_FAILED'
        else:
            subdir = '_SUCCESS'
        blob_filename = os.path.join(logs_path,
                                     subdir,
                                     os.path.splitext(json_filename)[0] + '.log')
        log_entry = """\
{separator}
job id: {job_id}
date: {date}
{separator}
status: {status}
{separator}
contents:
{contents}
{separator}
standard output:
{stdout}
{separator}
standard error:
{stderr}
{separator}
Docker details:
{docker_details}
Extra details:
{extra_details}
        """
        log_entry = log_entry.format(job_id=json_filename,
                                     status=status,
                                     contents=json.dumps(contents, indent=2),
                                     stdout=stdout,
                                     stderr=stderr,
                                     separator='='*80,
                                     date=datetime.datetime.now(),
                                     docker_details=docker_details,
                                     extra_details=extra or '')
        # Redact any secret information
        for key, value in secret.items():
            log_entry = log_entry.replace(value, '...redacted...')

        logs_blob = logs_bucket.blob(blob_filename)
        # use retryable function since this could fail
        upload_string(logs_blob, log_entry)
        self.logger.info('Saved logs in %s', logs_blob.path)

        # Set the owner to this log file
        acl = logs_blob.acl
        self.logger.info('Setting permissions for %s', self.config['owner_email'])
        acl.user(self.config['owner_email']).grant_owner()
        acl.save()

    def command_line(self, job, secret=None):
        env = {}
        secret = secret or {}
        interpreter = job['Interpreter']

        # Copy the job as json in /mnt/json_temp
        with open(os.path.join(self.dirs['json_temp'].name, 'job.json'), 'w') as f:
            json.dump(job, f)

        # [RES-215]: to support multiple interpreter versions, it is easier
        # to have a unique way to execute a job.
        command = [
            'run_job.sh',
            '/mnt/json_temp/job.json',
        ]

        # Add license information for NeuroRT jobs
        if interpreter in ('NeuroRT', 'NeuroRTTool'):
            env['CLOUDTS_DEBUG'] = '1'  # A flag for the NeuroRT entrypoint to print some extra info
            for key in ('NEURORT_LICENSE_USER', 'NEURORT_LICENSE_KEY'):
                env[key] = secret.get(key, '')

        return command, env


@retry(count=5)
def _try_remove_container(container, *, logger):
    """Remove a Docker container"""
    try:
        container.remove(force=True)
    except Exception:
        logger.debug('Attempted to remove container and failed, retrying...')
        raise


def _create_container(client, image_name, *, logger, **kwargs):
    logger.info('Creating container for image %s', image_name)
    container = client.api.create_container(image_name, **kwargs)
    return client.containers.get(container['Id'])


def _create_pull_container(client, image_name, *, logger, **kwargs):
    # TODO: as mentioned by jlegeny, there may be a better way than this
    # approach, something like checking if the image exists instead of "asking
    # for forgiveness" when creating one.  We would need to read more on
    # https://docker-py.readthedocs.io/en/stable/api.html
    import docker.errors
    try:
        return _create_container(client, image_name, logger=logger, **kwargs)
    except docker.errors.ImageNotFound:
        logger.info('Image %s not found, attempting to pull it', image_name)
        _pull_image(client, image_name, logger=logger)

    try:
        return _create_container(client, image_name, logger=logger, **kwargs)
    except docker.errors.ImageNotFound as ex:
        logger.error('Could not pull image %s', image_name, exc_info=ex)
        raise


def _pull_image(client, image_name, *, logger):
    # First, ensure that we login to the docker / google registry
    logger.info('Pulling image %s', image_name)
    username = '_token'
    token_cmd = ['gcloud', 'auth', 'print-access-token']
    password = subprocess.check_output(token_cmd).strip().decode('ascii')
    client.api.login(username, password, registry='https://gcr.io')

    # Pull the image
    for line in client.api.pull(image_name, stream=True):
        json_line = json.loads(line.decode('utf-8'))
        msg = json_line.get('status', '')
        if msg and not msg.endswith('\r\n'):
            logger.info(msg)

    logger.info('Pull of %s succeeded', image_name)


@log_wrapper
def work(args, *, config_broadcast, logger):
    # Import docker here so that docker is a dependency needed on the worker and
    # not the master
    import docker
    import docker.errors

    filename, content = args
    config = config_broadcast.value
    secret = config['secret']

    logger.info('WORK testing %s', config)

    # Initialize Google client
    gs_client = initialize_google_client()
    logger.info('Google client is %s', gs_client)

    # Initialize buckets
    manager = BucketManager(config, gs_client, logger)

    # Initialize docker
    docker_client = docker.client.from_env()

    # Read the job, we expect it to be a json representation
    logger.info('Got content from file %s: type is %s', filename, type(content))
    job = json.loads(content.decode('utf-8'))
    logger.info('Worker will attempt to execute the following JSON:\n%s',
                json.dumps(job, indent=2))

    # Check if the job is up to date and does not need to be run
    if not manager.is_stale(job):
        logger.info('Job is up to date, no need to run it again', job)
        manager.upload_logs(filename, job, None, None,
                            extra="Job's outputs are up to date",
                            secret=secret)
        return 0

    # Download all inputs and outputs to a volume
    logger.info('Downloading input files as necessary')
    job['Inputs'] = manager.resolve(job.get('Inputs', None), download=True)
    original_outputs = copy.deepcopy(job.get('Outputs', None))
    job['Outputs'] = manager.resolve(job.get('Outputs', None), download=False, can_be_missing=True)
    script_name = job['Name']
    # The script name should also be resolved, but not for NeuroRTTool or NCVT,
    # because its name is not a filename :-(
    if job['Interpreter'] not in ('NeuroRTTool', 'NCVT'):
        job['Name'] = manager.resolve(job['Name'], download=True)

    # basically do what pymensia.ncvt.jobs does
    interpreter = job.get('Interpreter', None)
    if not interpreter:
        raise WorkerException('Interpreter is not set')
    interpreter_version = job.get('InterpreterVersion', None)
    if not interpreter_version:
        if interpreter == 'NCVT':
            interpreter_version = '1.2-1.0'
        else:
            raise WorkerException('InterpreterVersion is not set')

    # Another special case. If the job is a NeuroRT, then also download the
    # .conf file if it exists
    if interpreter == 'NeuroRT':
        logger.info('NeuroRT needs a special .conf for %s', script_name)
        script_parts = script_name.split('.')
        script_parts[-1] = 'conf'
        conf_name = '.'.join(script_parts)
        logger.info('Trying to resolve %s', conf_name)
        manager.resolve(conf_name, download=True, can_be_missing=True)

    command, environment = manager.command_line(job, secret)
    logger.info('command is %s', command)

    volume_binds = manager.docker_volumes
    volume_config = docker_client.api.create_host_config(binds=volume_binds)

    image_name = (
        '{url}/{project}/cloudts/interpreters/{interpreter}:{version}'
        .format(url='gcr.io',
                project=config['project_id'],
                interpreter=interpreter.lower(),
                version=interpreter_version)
    )

    # Get a container from the interpreter configuration
    container = _create_pull_container(
        docker_client, image_name,
        logger=logger,
        command=command,
        environment=environment,
        volumes=list(volume_binds.keys()),
        host_config=volume_config,
        working_dir='/tmp',
        stop_timeout=60 * 5,  # Stop timeout in seconds. TODO: this is not the real timeout.
    )
    container.start()
    exit_status = container.wait()   # TODO: wait until timeout else kill
    # TODO: the timeout code should be as follows:
    # try:
    #     container.start()
    #     exit_status = container.wait()
    # except XXX: # It seems this should be a requests.exceptions.ReadTimeout
    #     # Handle the error
    #     xx

    # manage the logs
    manager.upload_logs(filename, job, exit_status, container, secret=secret)

    # Delete the container. Note that this is a retry-decorated operation
    # because this tends to fail sometimes with docker (I don't know why)
    try:
        _try_remove_container(container, logger=logger)
    except:
        logger.warning('Could not remove container', exc_info=True)

    if exit_status:
        logger.error('Container command failed with error %s', exit_status)
        return exit_status

    # Copy outputs to their destinations
    logger.info('Uploading generated files')
    manager.upload(original_outputs)

    logger.info('Worker finished successfully')
    return 0


def get_stages(tup):
    filename, content = tup
    parts = filename.split('/')
    stage = int(parts[-2].split('_')[-1])
    return stage, (filename, content)


def to_list(a):
    return [a]


def append(a, b):
    a.append(b)
    return a


def extend(a, b):
    a.extend(b)
    return a


def extract_tar(buffer):
    tar = tarfile.open(fileobj=io.BytesIO(buffer), mode="r")
    return [(x.name, tar.extractfile(x).read()) for x in tar if x.isfile()]


def save_textfile(rdd, url, file_owner=None):
    tmp_url = url + '_temp'
    rdd.saveAsTextFile(tmp_url)
    # Pragmatism: use hadoop command line to save a file
    read_cmd = ['hadoop', 'fs', '-cat', urljoin(tmp_url, 'part*')]
    write_cmd = ['hadoop', 'fs', '-put', '-f', '-', url]
    print('Command would be {} | {}'.format(' '.join(read_cmd),
                                            ' '.join(write_cmd)))
    read_p = subprocess.Popen(read_cmd, stdout=subprocess.PIPE)
    write_p = subprocess.Popen(write_cmd, stdin=read_p.stdout,
                               stdout=subprocess.PIPE)
    read_p.stdout.close()  # propagate SIGPIPE signal, according to docs
    copy_output = write_p.communicate()[0]
    print('copy output was', copy_output)
    delete_cmd = ['hadoop', 'fs', '-rm', '-r', tmp_url]
    print('Running command {}'.format(' '.join(delete_cmd)))
    subprocess.call(delete_cmd)

    # TODO: set permissions to _spark.log !
    if file_owner is not None:
        client = initialize_google_client()
        bucket = client.get_bucket(urllib.parse.urlparse(url).netloc)
        blob = bucket.get_blob(urllib.parse.urlparse(url).path.strip('/'))
        acl = blob.acl
        acl.user(file_owner).grant_owner()
        acl.save()


def main():

    print('(Master) this is Python {}'.format(sys.version.replace('\n', ' ')))
    print('args are', sys.argv)

    parser = argparse.ArgumentParser(description='CloudTS Spark application')
    parser.add_argument('--name', type=str, default='CloudTS Job app',
                        help='Name of the Spark application')
    # parser.add_argument('--job', type=str, nargs='+', action='append',
    #                     help='JSON jobs to execute')
    parser.add_argument('--job', type=str, required=True,
                        help='Tar file with the jobs to execute')
    parser.add_argument('--data-dir', type=str, required=True,
                        help='Data directory')
    parser.add_argument('--data-temp-dir', type=str, required=True,
                        help='Temporary data directory')
    parser.add_argument('--results-dir', type=str, required=True,
                        help='Results directory')
    parser.add_argument('--scripts-dir', type=str, required=True,
                        help='Scripts directory')
    parser.add_argument('--logs-dir', type=str, required=True,
                        help='Logs directory')
    parser.add_argument('--owner', type=str, required=True,
                        help='Owner to set to the files generated by this worker')
    parser.add_argument('--google-project-id', type=str, required=True,
                        help='Google cloud project identifier. Used to pull '
                        'docker images for interpreters')
    parser.add_argument('--secret', type=str, required=False,
                        help='Secret, encrypted parameters')
    opts = parser.parse_args()

    # Handle secret if present
    secret_dict = {}
    if opts.secret is not None:
        fernet = Fernet(SECRET_KEY)
        secret_dict = json.loads(fernet.decrypt(opts.secret.encode('ascii')).decode('utf-8'))

    # define spark session
    spark = SparkSession.builder \
                        .appName(opts.name) \
                        .getOrCreate()

    sc = spark.sparkContext
    # Configure to use the correct pool TODO: parametrize this?
    sc.setLocalProperty('spark.scheduler.pool', 'production')
    #sc.setLogLevel('DEBUG')
    print('(Master) this is Spark {}'.format(sc.version))
    print('This is where root files are', pyspark.SparkFiles.getRootDirectory())

    print('Creating broadcasted variables')
    config_broadcast = sc.broadcast({
        'data_dir': opts.data_dir,
        'data_temp_dir': opts.data_temp_dir,
        'results_dir': opts.results_dir,
        'scripts_dir': opts.scripts_dir,
        'logs_dir': opts.logs_dir,
        'owner_email': opts.owner,
        'project_id': opts.google_project_id,
        'secret': frozendict.frozendict(secret_dict),
    })
    print('Broadcasted variable is', config_broadcast)

    print('Collect/extract files from {}'.format(opts.job))
    stage_jobs = (
        sc
        .binaryFiles(opts.job)
        .flatMap(lambda file_tuple: extract_tar(file_tuple[1]))
        .map(get_stages)
        .combineByKey(to_list, append, extend)
        .sortByKey()
        .collect()
    )

    for stage_number, jobs in stage_jobs:
        n_stage = len(jobs)
        n_partitions = max(n_stage, 1)  # We want to force a lot of partitions
        print('Executing NCVT step {} of size {}'.format(stage_number, n_stage))

        # A verbose name for the RDD to see it in the storage tab:
        names_collection = collections.Counter([
            json.loads(j[1].decode('utf-8')).get('Name', '').split('/')[-1]
            for j in jobs])
        names_str = ', '.join(['{0} x {1}'.format(name, count)
                               for name, count in names_collection.most_common()])

        # Apply the work function
        result_rdd = (
            sc
            # Parallelize in partitions with one element so that a fail can be
            # reworked easily
            .parallelize(jobs, numSlices=n_partitions)
            .map(functools.partial(work, config_broadcast=config_broadcast))
            .setName('NCVT results RDD stage {} - {}'.format(stage_number, names_str))
            .persist()
        )

        print('RDD of step has {} partitions'.format(result_rdd.getNumPartitions()))
        status = result_rdd.map(lambda tup: tup[0]).collect()
        print('All status:', status)
        print('status summary:', collections.Counter(status))
        if any(s != 0 for s in status):
            print('Aborting, there was an error, one of the status was not 0')
            error_logs = (
                result_rdd
                .filter(lambda tup: tup[0] != 0)
                .map(lambda tup: 'Spark details log:\n{sep}\n{content}\n'
                     .format(sep='='*80, content=tup[1]))
                .coalesce(64)
            )

            spark_logfile = urljoin(opts.logs_dir, 'spark.log')
            save_textfile(error_logs, spark_logfile, file_owner=opts.owner)
            raise MasterException('Job failed')

        # [RES-203]
        # Unpersist the RDD so that executors may be released.
        # Also unpersist the broadcasted variable (it will be re-sent if needed)
        # so that executors may be released
        result_rdd.unpersist()
        config_broadcast.unpersist()

    # Stop SparkContext
    sc.stop()


if __name__ == "__main__":
    main()
