# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import collections
import datetime
import io
import json
import logging
import tarfile
import tempfile
import os
import os.path as osp
import re
import sys
import urllib
from posixpath import join as urljoin

import backoff
from cloudts.client.google import RobustGoogleStorageClient
from oauth2client.client import GoogleCredentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from rest_framework import status
from cryptography.fernet import Fernet

from cloudts.api.job.exceptions import JobAPIException, ConfigurationException, RestAPIException


SECRET_KEY = b'3VM1cm_NHYzUbNNFGdlbzTg6w3TJsUcivZo6h4kmEHk='

# The following would make the httplib2 show the requests being made to the
# Google REST API. Use it for debugging purposes
#import httplib2; httplib2.debuglevel = 4

logger = logging.getLogger(__name__)


# Some constant configuration for Google DataProc
REGION = 'global'
API_NAME = 'dataproc'
API_VERSION = 'v1beta2'


class GoogleJobBackend():
    """ Backend implementation for interacting with Google cloud """

    def __init__(self, *, project_name, cluster_name, bucket_name, basedir_cloud, json_credentials):
        """initialises google backend

        This class is the connection to the Google Cloud platform.
        It is possible to upload files, to submit jobs (our use case Spark jobs)
        and to derive the execution status of the job

        Parameters
        ----------
        project_name: str
            name of google project

        cluster_name: str
            name of google cluster

        bucket_name: str
            name of google bucket

        basedir_cloud: str
            base directory to upload files to in the google cloud storage

        json_credentials: str
            path to the json file which provides the authentication credentials

        """
        self.project_name = project_name
        self.cluster_name = cluster_name
        self.bucket_name = bucket_name
        self.basedir_cloud = basedir_cloud
        self.json_credentials = json_credentials

        try:
            self.credentials = GoogleCredentials.from_stream(json_credentials)
        except:
            logger.error('Could not get credentials from file {}'.format(json_credentials))
            raise ConfigurationException('Could not read google credentials')

        # create objects to communicate with google cloud platform
        # client: to send Storage objects
        self.client = RobustGoogleStorageClient.from_service_account_json(self.json_credentials)
        self.bucket = self.client.get_bucket(self.bucket_name)

        # service: to send cluster jobs
        # to obtain the api_name and api_version: https://developers.google.com/api-client-library/python/apis/
        self._init_service()  # changed to a function that is retryable

    @backoff.on_exception(backoff.expo, HttpError, max_tries=5)
    def _init_service(self):
        self.service = build(API_NAME, API_VERSION, credentials=self.credentials,
                             cache_discovery=False)

    def upload_file(self, *, bucket=None, source, destination, owner=None):
        """ upload local file to Google cloud storage

        Parameters
        ----------
        source: str
            filename to upload

        destination: str
            filename path in google storage where to upload file to
        """
        if bucket is None:
            bucket = self.bucket
        logger.info('uploading file %s to %s on bucket %s', source, destination, bucket)
        blob = bucket.blob(destination)
        if hasattr(source, 'read'):
            blob.upload_from_file(source, rewind=True)
        else:
            blob.upload_from_filename(source)
        if owner is not None:
            acl = blob.acl
            acl.user(owner).grant_owner()
            acl.save()

    #def submit(self, *, job_id_name, list_path_json):
    def submit(self, *, job_instance, workspace_instance, job_list, query=None, dataframe=None, project_id=None):
        """Submits the Pyspark job to the cluster, assuming `filename` has
           already been uploaded to `bucket_name`

        Parameters
        ----------
        job_id_name: str
            identifier name for the job

        list_path_json: list
            list of the paths of all json files (jobs) in the google storage
            NEW: list of stages

        Returns
        -------
        int
            job id
        """

        # Obtain user-dependent configuration
        user = workspace_instance.workspace_model.owner
        target_project_name, target_cluster_name = self._get_user_job_config(user)

        # check that the files of all jobs
        for stage in job_list:
            self._check_job([j[1] for j in stage])

        # target_dir is the base directory for all self.upload_file. It will
        # have all the files for this job will be saved (json files), this does
        # not include the gs:// part because the self.bucket already has it
        target_dir = urljoin(self.basedir_cloud, job_instance.backend_id)

        # Create secret
        secret_dict = {}
        for key in ('NEURORT_LICENSE_USER', 'NEURORT_LICENSE_KEY'):
            if key in os.environ:
                secret_dict[key] = os.environ[key]
        fernet = Fernet(SECRET_KEY)
        # symmetric encryption of sensitive information:
        # dump to json (str), then encode to utf-8 (bytes),
        # encrypt (bytes, in base64 charset), then decode to ascii (str)
        secret = fernet.encrypt(json.dumps(secret_dict).encode('utf-8')).decode('ascii')

        # Send the worker code
        worker_target = urljoin(target_dir, 'google_spark_worker.py')
        self.upload_file(destination=worker_target,
                         source=osp.join(osp.dirname(__file__), 'google_spark_worker.py'))
        main_file = 'gs://{bucket}/{path}'.format(bucket=self.bucket_name,
                                                  path=worker_target)

        # Send the job files
        target_file = urljoin(target_dir, 'jobs.tar')
        tmp_buffer = io.BytesIO()
        tar(job_list, tmp_buffer)
        tmp_buffer.flush()
        self.upload_file(source=tmp_buffer, destination=target_file)

        #input_dir = urljoin(target_dir, 'json')
        #file_uris = {urljoin('gs://', self.bucket.name, input_dir, '**')}
        file_uris = [urljoin('gs://', self.bucket.name, target_file)]
        # Note: unfortunately, is not possible to batch the upload of many files
        # Perhaps if we invest some time in researching, thinking and testing,
        # we could do this faster with an asyncio approach
        #logger.info('job instance is %s %s', job_instance, type(job_instance))
        logs_dir = urljoin(workspace_instance.data_uri, 'logs',
                          '{}-step_{:03d}-{}'.format(
                              datetime.datetime.now().strftime('%Y%m%d-%H%M%S'),
                              job_instance.stage_id,
                              job_instance.backend_task.task_id))

        # Copy the job file to the logs for easier debugging
        logs_bucket_url = urllib.parse.urlparse(urljoin(logs_dir, 'jobs.tar'))
        logs_bucket = self.client.get_bucket(logs_bucket_url.netloc)
        self.upload_file(bucket=logs_bucket,
                         source=tmp_buffer, destination=logs_bucket_url.path.lstrip('/'),
                         owner=workspace_instance.workspace_model.owner.email)

        # Copy the SQL and results on the log directory for debugging
        if query is not None:
            logs_sql_url = urllib.parse.urlparse(urljoin(logs_dir, 'query.sql'))
            with tempfile.NamedTemporaryFile('w') as f:
                f.write(query)
                f.flush()
                self.upload_file(bucket=logs_bucket,
                                 source=f.name,
                                 destination=logs_sql_url.path.lstrip('/'),
                                 owner=workspace_instance.workspace_model.owner.email)
        if dataframe is not None:
            logs_csv_url = urllib.parse.urlparse(urljoin(logs_dir, 'db.csv'))
            with tempfile.NamedTemporaryFile('w') as f:
                dataframe.to_csv(f.name, index=False)
                self.upload_file(bucket=logs_bucket,
                                 source=f.name,
                                 destination=logs_csv_url.path.lstrip('/'),
                                 owner=workspace_instance.workspace_model.owner.email)

        # Copy the sequence to the logs for easier debugging
        json_bucket_url = urllib.parse.urlparse(urljoin(logs_dir, 'sequence.json'))
        with tempfile.NamedTemporaryFile('w') as f:
            json.dump(job_instance.content, f, indent=2)
            f.flush()
            self.upload_file(bucket=logs_bucket,
                             source=f.name,
                             destination=json_bucket_url.path.lstrip('/'),
                             owner=workspace_instance.workspace_model.owner.email)

        # Prepare command line arguments for google-spark-worker.py
        app_name = 'CloudTS job {}'.format(job_instance.backend_task.task_id)
        args = ['--name', app_name,
                '--job', urljoin('gs://', self.bucket_name, target_file),
                '--data-dir', workspace_instance.backend.data_uri.rstrip('/'),
                '--data-temp-dir', urljoin(workspace_instance.data_uri, 'data_temp'),
                '--results-dir', urljoin(workspace_instance.data_uri, 'results'),
                '--scripts-dir', urljoin(workspace_instance.data_uri, 'scripts'),
                '--logs-dir', logs_dir,
                '--owner', workspace_instance.workspace_model.owner.email,
                '--google-project-id', project_id,
                '--secret', secret,
        ]

        # Prepare request to the Dataproc API. More doc in
        # https://cloud.google.com/dataproc/docs/reference/rest/v1beta2/projects.regions.jobs
        job_details = {
            'job': {
                'reference': {
                    'projectId': target_project_name,
                    'jobId': job_instance.backend_id,
                },
                'placement': {
                    'clusterName': target_cluster_name
                },
                'pysparkJob': {
                    'mainPythonFileUri': main_file,
                    'args': args,
                    'fileUris': file_uris,
                    'loggingConfig': {
                        'driverLogLevels': {  # Consider this for finer logging and to understand where files are saved
                            'root': 'INFO',
                            # there is a bug on datastream that shows a warn,
                            # but consider put this back if an error occurs on
                            # the cluster
                            'org.apache.hadoop.hdfs.DataStreamer': 'ERROR', 
                        }
                    }
                }
            }
        }
        logger.info('Job representation:\n%s', json.dumps(job_details, indent=2))
        result = self.service.projects().regions().jobs().submit(
            projectId=target_project_name,
            region=REGION,
            body=job_details).execute()
        job_id = result['reference']['jobId']

        logger.info('Submitted job ID {}'.format(job_id))
        return job_id

    def submit_pyspark(self, *, job_instance, workspace_instance, job):
        # Obtain user-dependent configuration
        user = workspace_instance.workspace_model.owner
        target_project_name, target_cluster_name = self._get_user_job_config(user)

        # check that the files of all jobs
        job_id, job_contents = job
        #from celery.contrib import rdb; rdb.set_trace()
        self._check_job([job_contents])

        # Prepare args: save the argument as a JSON so that the call can be
        # pyspark script.py --json-file url
        target_dir = urljoin(self.basedir_cloud, job_instance.backend_id)
        target_file = urljoin(target_dir, 'job.json')
        tmp_buffer = io.BytesIO(json.dumps(job_contents).encode('utf-8'))
        self.upload_file(source=tmp_buffer, destination=target_file,
                         owner=workspace_instance.workspace_model.owner.email)
        args = ['--json-file', urljoin('gs://', self.bucket_name, target_file)]
        # args = [json.dumps(job_contents).replace('"', "'")]

        # Prepare request to the Dataproc API. More doc in
        # https://cloud.google.com/dataproc/docs/reference/rest/v1beta2/projects.regions.jobs
        job_details = {
            'job': {
                'reference': {
                    'projectId': target_project_name,
                    'jobId': job_instance.backend_id,
                },
                'placement': {
                    'clusterName': target_cluster_name
                },
                'pysparkJob': {
                    'mainPythonFileUri': job_contents['Name'],
                    # 'args': 'gs://{}/{}'.format(self.bucket_name, input_dir),
                    'args': args,
                    # 'fileUris' : list(file_uris),
                    'loggingConfig': {
                        'driverLogLevels': {
                            # Consider using 'INFO' for finer logging, to
                            # understand where files are saved, and to read
                            # important messages related to Spark
                            # 'root': 'INFO',
                            # there is a bug on datastream that shows a warn,
                            # but consider put this back if an error occurs on
                            # the cluster
                            'org.apache.hadoop.hdfs.DataStreamer': 'ERROR',
                        }
                    }
                }
            }
        }
        logger.info('Job representation:\n%s', json.dumps(job_details, indent=2))
        result = self.service.projects().regions().jobs().submit(
            projectId=target_project_name,
            region=REGION,
            body=job_details).execute()
        job_id = result['reference']['jobId']

        logger.info('Submitted job ID {}'.format(job_id))
        return job_id

    def _check_job(self, jobs):
        """Verify that a job definition is correct

        At the moment, correct means that the script, inputs and outputs are all
        files in a Google bucket

        """
        for i, job in enumerate(jobs):
            logger.debug('checking %s', job)
            if not isinstance(job, collections.Mapping):
                raise JobAPIException('Invalid job representation', status_code=400)
            if 'Name' not in job:
                raise JobAPIException('Missing job script (Name)', status_code=400)
            if not valid_name(job['Interpreter'], job['Name']):
                raise JobAPIException(detail='Job script (Name) is not valid: {}'
                                      .format(job['Name']),
                                      code='error',
                                      status_code=400)
            check(job.get('Inputs', {}), 'Input', i)
            check(job.get('Outputs', {}), 'Output', i)

    def cancel(self, *, job_id, user):
        """ Cancel job with id <job_id>

        Parameters
        ----------
        job_id: int
            ID of the job to be cancelled

        Returns
        -------
        string
            Status of this job
        """
        # Obtain user-dependent configuration
        target_project_name, _ = self._get_user_job_config(user)

        # This has no chance to work today; the view is disabled.
        raise NotImplementedError('Canceling disabled')

        result = self.service.projects().regions().jobs().cancel(
            projectId=target_project_name,
            region=REGION,
            jobId=job_id,
            body='')

        # Bug in service (library google-api-python-client), remove body
        # Issue: https://github.com/google/google-api-python-client/issues/416
        if result.body is not None:
            result.body = None
            result.body_size = 0

        result = result.execute()

        # Handle exceptions
        if result['status']['state'] == 'ERROR':
            details = result['status'].get('details', 'No details')
            raise JobAPIException(detail=details,
                                  code='error',
                                  status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else :
            return result['status']['state']

    def status(self, *, job_id, user):
        """ Get status of the job with id <job_id>

        Parameters
        ----------
        job_id: int
            ID of the job to ask the status

        Returns
        -------
        string
            Status of this job

        details
            More details if the backend can provide them
        """

        # Obtain user-dependent configuration
        target_project_name, _ = self._get_user_job_config(user)

        try:
            request = self.service.projects().regions().jobs().get(
                projectId=target_project_name,
                region=REGION,
                jobId=job_id)
            response = request.execute()
        except:
            logger.warning('Error communicating with Google', exc_info=sys.exc_info())
            raise RestAPIException(detail='Error communicating with dataproc',
                                   code='error',
                                   status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # logger.info('Response is %s', response)
        return response['status']['state'], response['status']

    def _get_user_job_config(self, user):
        try:
            user_job_config = user.job_config.config
            target_project_name = user_job_config['project_name']
            target_cluster_name = user_job_config['cluster_name']
        except:
            logger.debug('Did not find user-specific job backend '
                         'configuration for user %s. Using defaults.',
                         user)
            target_project_name = self.project_name
            target_cluster_name = self.cluster_name
        return target_project_name, target_cluster_name


###############################################################################
def valid_name(interpreter, name):
    # NeuroRTTool is an exception
    if interpreter == 'NeuroRTTool':
        return re.match('mensia-\w+tool', name)
    # NCVT Name is also a script name: very unpractical
    if valid_url(name):
        return True
    # These are the known and supported NCVT internal jobs
    if interpreter == 'NCVT':
        return name in ('BlockReduce', 'MetadataCollect', 'FoldReduce', 'averageValidationSet')
    # Anything else is invalid
    return False


def valid_url(url):
    if isinstance(url, list):
        return all(valid_url(u) for u in url)
    if not isinstance(url, str):
        return False
    if not url.startswith('gs://'):
        return False
    return True


def check(mapping, map_name, number):
    for k, v in mapping.items():
        if not valid_url(v):
            raise JobAPIException('{name}[{index}] {key} is invalid for cluster execution'
                                  .format(name=map_name, key=k, index=number),
                                  status_code=400)


def tar(job_stages, fileobj):
    tmp_tar = tarfile.open(mode='w', fileobj=fileobj)
    for stage_number, stage in enumerate(job_stages):
        for filename, content in stage:
            with tempfile.NamedTemporaryFile('w', encoding='utf-8') as tmp_json:
                json.dump(content, tmp_json)
                tmp_json.flush()
                tmp_tar.add(tmp_json.name,
                            arcname=urljoin('stage_{}'.format(stage_number),
                                            filename))
    tmp_tar.close()
