# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" CloudTS Data API metadata related models """

from collections import OrderedDict

from django.core.exceptions import PermissionDenied
from django.db import models
from jsonfield import JSONField

from cloudts.api.data.models import Workspace


class Metadata(models.Model):
    """Metadata model

    The representation used for the metadata implementation that relies on a
    postgres database

    """
    # Implictly, the following field "id" is added by Django, and we need it for
    # the workspace.start_id field
    # id = models.AutoField(primary_key=True)
    # TODO: perhaps on_delete=models.DO_NOTHING ?
    workspace = models.ForeignKey(Workspace,
                                  verbose_name='Workspace',
                                  related_name='metadata',
                                  on_delete=models.CASCADE,
                                  null=True,
                                  editable=False)
    file_handler = models.UUIDField(verbose_name='File handler',
                                    null=False,
                                    editable=False)
    family = models.CharField(verbose_name='Family',
                              max_length=127,
                              null=False,
                              blank=False)
    version = models.PositiveIntegerField(verbose_name='Version',
                                          null=False)
    kv_store = JSONField(verbose_name='Unstructured metadata',
                         null=False)


    def get_metadata_dict(self):
        """Get the metadata structured as family:contents

        This returns a copy of the metadata, ordered in a certain way: First the
        id, then anything else

        """
        ordered = OrderedDict()
        ordered['id'] = self.kv_store['id']  # pylint: disable=unsubscriptable-object
        ordered.update(self.kv_store.copy()) # pylint: disable=no-member
        return {self.family : ordered}

    def __str__(self):
        fmt = 'Metadata file={0} workspace={1} family={2} version={3} data={4}'
        return fmt.format(self.file_handler, self.workspace.pk, self.family,
                          self.version, self.kv_store)

    class Meta:
        unique_together = ('workspace', 'file_handler', 'family', 'version')
        # We will force the name of this table for the view creation of the
        # FileRecord model
        db_table = 'cloudts_metadata'


class FileRecord(models.Model):
    """File record helper model.

    This model uses a view on the Metadata model that has its handler and the
    information needed to determine if the file is already known by CloudTS.

    """

    file_handler = models.UUIDField(verbose_name='File handler',
                                    editable=False)
    workspace = models.ForeignKey(Workspace,
                                  verbose_name='Workspace',
                                  related_name='filerecords',
                                  on_delete=models.DO_NOTHING,
                                  null=True,
                                  editable=False)
    url = models.CharField(verbose_name='File URL',
                           max_length=1024,
                           editable=False)
    hash = models.CharField(verbose_name='Checksum of the file',
                            max_length=256,
                            editable=False)
    size = models.PositiveIntegerField(verbose_name='Size in bytes of the file')

    def __str__(self):
        fmt = 'file {file_handler} on workspace {ws}, url={uri}, hash={hash}, size={size} bytes'
        return fmt.format(file_handler=self.file_handler,
                          ws=self.workspace.pk,
                          uri=self.url,
                          hash=self.hash, size=self.size)

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ,unused-argument
        """ Saving this model is not permitted; it will raise an exception """
        raise PermissionDenied('File records should not be modified')

    class Meta:
        # Do not let django manage this model because it will be a database view
        managed = False
        # This is the database view that should be used for this model
        db_table = 'filerecord_view'
