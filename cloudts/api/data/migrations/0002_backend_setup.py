# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cloudts_data', '0001_initial'),
    ]

    operations = [
        ## Add the view for the file records
        migrations.RunSQL(
            """
            DROP VIEW IF EXISTS filerecord_view;

            CREATE OR REPLACE VIEW filerecord_view AS
            SELECT
              id,
              file_handler,
              workspace_id,
              kv_store->>'hash' AS hash,
              CAST(kv_store->>'size' AS INTEGER) AS size,
              kv_store->>'url' AS url
            FROM cloudts_metadata
            WHERE
              family = 'cloudts';
            """),

        ## Install the aggregate function to merge json entries
        migrations.RunSQL(
            """
            DROP AGGREGATE IF EXISTS jsonb_merge(jsonb);
            CREATE AGGREGATE jsonb_merge(jsonb) (
                SFUNC = jsonb_concat(jsonb, jsonb),
                STYPE = jsonb,
                INITCOND = '{}'
            );
            """
        ),
    ]
