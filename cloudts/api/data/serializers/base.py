# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" Common objects for all Data API serializers """

import collections
import json

from rest_framework.serializers import Field, Serializer


class JSONSerializerField(Field):
    """ Serializer for a JSONField

    This serializer is needed to make JSONFields writable.

    """
    def to_internal_value(self, data):
        """ Transform the incoming primitive data into a native value. """
        return data

    def to_representation(self, value):
        """ Transform the outgoing native value into primitive data. """
        if isinstance(value, dict):
            return value
        return json.loads(value, object_pairs_hook=collections.OrderedDict)


class BasicSerializer(Serializer):
    """ A basic DRF serializer that does not save/create objects """

    def update(self, instance, validated_data):
        raise RuntimeError('%s does not update'%self.__class__.__name__)

    def create(self, validated_data):
        raise RuntimeError('%s does not create'%self.__class__.__name__)
