# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
# ###################################

################################################################################
# Script to create user for a CloudTS local backend
#

set -x

USER=$1
echo "Creating environment for user $1 ..."

adduser -D -s /sbin/nologin $1 $1
