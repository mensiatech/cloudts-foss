# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import os
import tempfile
import uuid

from mock import patch

from django.test import TestCase, override_settings
from django.contrib.auth.models import User, AnonymousUser

from rest_framework.test import APIClient, RequestsClient, APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status

from cloudts.api.data.backends import get_backend
from cloudts.api.data.backends.base import AbstractDataWorkspace

TMPDIR = tempfile.TemporaryDirectory()
TEST_SETTINGS = dict(
    CLOUDTS_CONFIG = {
        'data_backend': {
            'ENGINE': 'cloudts.api.data.backends.local.LocalBackend',
            'OPTIONS': {
                'basedir': TMPDIR.name,
                'enforce_permissions': False,
            },
        }
    },
    SECURE_SSL_REDIRECT = False
)


@override_settings(**TEST_SETTINGS)
class AuthenticationTest(APITestCase):

    def setUp(self):
        self.known_user = User.objects.create_user(username='tester',
                                                   password='secret',
                                                   email='test@example.com')
        self.client.defaults['wsgi.url_scheme'] = 'https'

    def test_token_authentication(self):
        input_json = {'username': 'tester',
                      'password': 'secret'}
        response = self.client.post('/v1/auth/token/', data=input_json)
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         'Could not get a token')

        output_token = response.json()['token']
        expected_token = self.known_user.auth_token
        self.assertEqual(output_token, expected_token.key,
                         'Unexpected token value')

    def test_unknown_user(self):
        response = self.client.get('/v1/data/workspaces/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED,
                         'Unknown user should not be able to access protected '
                         'resource')

    def test_protected_resources(self):
        endpoints = {
            '/v1/data/workspaces/': 'get',
            '/v1/data/workspaces/': 'post',
            '/v1/data/workspaces/1/': 'get',
            '/v1/data/workspaces/1/commit/': 'post',
        }
        for url in endpoints:
            method = endpoints[url]
            func = getattr(self.client, method)
            response = func(url)
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED,
                             'Endpoint {} is not correctly protected'.format(url))


@override_settings(**TEST_SETTINGS)
class WorkspaceCreateTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='tester',
                                             password='secret',
                                             email='nobody@example.com')
        self.client.defaults['wsgi.url_scheme'] = 'https'
        self.client.force_authenticate(user=self.user)

    def tearDown(self):
        self.client.force_authenticate(user=None)

    def test_get_workspace(self):
        response = self.client.get('/v1/data/workspaces/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_workspace(self):
        response = self.client.post('/v1/data/workspaces/',
                                    {'name': 'test_name_1'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_workspace_with_families(self):
        response = self.client.post('/v1/data/workspaces/',
                                    {'name': 'test_name_2',
                                     'families': {'cloudts': 0}
                                    },
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

@override_settings(**TEST_SETTINGS)
class WorkspaceFilesTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='tester',
                                             password='secret',
                                             email='nobody@example.com')
        self.client.defaults['wsgi.url_scheme'] = 'https'
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/v1/data/workspaces/', format='json')
        self.workspace_id = response.json()['id']

    def tearDown(self):
        self.client.force_authenticate(user=None)


    def test_add_file(self):
        fixed_id = str(uuid.uuid4())
        with tempfile.NamedTemporaryFile() as f, \
             patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=lambda: fixed_id):
            f.write(uuid.uuid4().bytes)
            f.flush()
            f.seek(0)
            response = self.client.post('/v1/data/files/',
                                        {
                                            'workspace': self.workspace_id,
                                            'file': f
                                        },
                                        format='multipart').json()
            self.assertEqual(response['url'].split('/')[-2], fixed_id,
                             'Unexpected file handle value')
            self.assertEqual(response['workspace'], self.workspace_id,
                             'File not created in workspace')


@override_settings(**TEST_SETTINGS)
class WorkspaceFileMetadataTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='tester',
                                             password='secret',
                                             email='nobody@example.com')
        self.client.defaults['wsgi.url_scheme'] = 'https'
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/v1/data/workspaces/', format='json')
        self.workspace_id = response.json()['id']

        with tempfile.NamedTemporaryFile() as f:
            f.write(uuid.uuid4().bytes)
            f.flush()
            f.seek(0)
            add_response = self.client.post('/v1/data/files/',
                                            {
                                                'workspace': self.workspace_id,
                                                'file': f
                                            },
                                            format='multipart').json()
            self.file_id = add_response['url'].split('/')[-2]

    def tearDown(self):
        self.client.force_authenticate(user=None)

    def test_file_details(self):
        response = self.client.get('/v1/data/files/{}/'.format(self.file_id),
                                   {
                                       'workspace': self.workspace_id
                                   }).json()
        self.assertTrue('metadata' in response,
                        'Metadata not in response')
        self.assertTrue('cloudts' in response['metadata'],
                        'Missing default cloudts family')
        self.assertEqual(response['workspace'], self.workspace_id,
                         'File not in expected workspace')

    def test_add_metadata(self):
        new_metadata = {
            'family': {
                'a': 'a',
                'b': 'bbb',
                'c': None,
                'd': {'d1': 1.1, 'd2': 2.2},
                'e': [1,2,3],
            }
        }
        response = self.client.patch('/v1/data/files/{}/'.format(self.file_id),
                                     {
                                         'workspace': self.workspace_id,
                                         'metadata': new_metadata
                                     },
                                     format='json').json()
        self.assertIn('metadata', response)

        metadata = response['metadata']
        self.assertIn('cloudts', metadata)
        self.assertIn('family', metadata)

        cloudts_metadata = response['metadata']['cloudts']
        family_metadata = response['metadata']['family']
        self.assertEqual(cloudts_metadata['id'], family_metadata['id'])
        self.assertDictContainsSubset(new_metadata['family'], family_metadata)
