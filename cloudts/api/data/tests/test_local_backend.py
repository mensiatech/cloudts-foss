# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


import copy
import glob
import hashlib
import itertools
import json
from mock import patch
import os
import pwd
import tempfile
import unittest
import uuid
import collections
import pickle
import pandas as pd

from django.contrib.auth.models import User
from django.test import TestCase, override_settings

from cloudts.api.data.backends.base import AbstractDataWorkspace
from cloudts.api.data.backends.local import LocalBackend, LocalWorkspace
from cloudts.api.data.backends.exceptions import CommitConflictException


def _user_is_root():
    """ Determine if user is root """
    import os
    return (os.getuid() == 0)


# Django settings for unit tests on LocalBackend
TMPDIR = tempfile.TemporaryDirectory()
TEST_SETTINGS = dict(
    CLOUDTS_CONFIG = {
        'data_backend': {
            'ENGINE': 'cloudts.api.data.backends.local.LocalBackend',
            'OPTIONS': {
                'basedir': TMPDIR.name,
                'enforce_permissions': _user_is_root(),
            },
        }
    }
)


@override_settings(**TEST_SETTINGS)
class LocalBackendTests(TestCase):

    WorkspaceMockModel = collections.namedtuple('Workspace',
                                                ('name',
                                                 'owner',
                                                 'families'))

    def setUp(self):
        self.user = User.objects.create_user(username='tester',
                                             password='secret',
                                             email='test@example.com')
        self.tmpdir = tempfile.TemporaryDirectory()
        self.backend = LocalBackend(basedir=self.tmpdir.name,
                                    enforce_permissions=False)

    def test_create_directories(self):
        model = self.WorkspaceMockModel('test_create_dirs', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        self.assertTrue(os.path.exists(self.tmpdir.name),
                        'Base backend directory does not exist')
        datadir = os.path.join(self.tmpdir.name, 'data')
        self.assertTrue(os.path.exists(datadir) and os.path.isdir(datadir),
                        'Backend data directory does not exist')
        metadatadir = os.path.join(self.tmpdir.name, 'metadata')
        self.assertTrue(os.path.exists(metadatadir) and os.path.isdir(metadatadir),
                        'Backend metadata directory does not exist')

    def test_create_workspace(self):
        model = self.WorkspaceMockModel('test_create', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        self.assertTrue(os.path.exists(workspace.datadir) and
                        os.path.isdir(workspace.datadir),
                        'Workspace data directory not created')
        self.assertTrue(os.path.exists(workspace.metadatadir) and
                        os.path.isdir(workspace.metadatadir),
                        'Workspace data directory not created')

    def test_add_file(self):
        model = self.WorkspaceMockModel('test_add_file', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        # Create a file named file_xxx.bin with random contents, directly in the
        # data dir of the workspace
        random_file = tempfile.NamedTemporaryFile(dir=workspace.data_dir)
        random_file.write(uuid.uuid4().bytes)
        random_file.flush()

        file_uri, file_metadata = workspace.add_file(random_file.name,
                                                     owner=self.user.username,
                                                     permissions=0o600)
        data_uri = os.path.join(self.backend.datadir,
                                file_uri.split('/')[-1])

        self.assertTrue(os.path.exists(data_uri), 'File copied outside datadir')

        with open(random_file.name, 'rb') as f1, open(data_uri, 'rb') as f2:
            bytes_original = f1.read()
            bytes_copy = f2.read()
            self.assertEqual(bytes_original, bytes_copy,
                             'Copied contents do not match')

        self.assertTrue(file_uri.startswith(workspace.datadir),
                        'File copied outside datadir')

    def test_add_file_stream(self):
        model = self.WorkspaceMockModel('test_add_file_stream', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        # Create a file named file_xxx.bin with random contents, flush it and
        # then use the stream to add a file-like object
        random_file = tempfile.NamedTemporaryFile()
        random_file.write(uuid.uuid4().bytes)
        random_file.flush()
        random_file.seek(0)

        file_uri, file_metadata = workspace.add_file(random_file,
                                                     owner=self.user.username,
                                                     permissions=0o600)
        data_uri = os.path.join(self.backend.datadir,
                                file_uri.split('/')[-1])

        self.assertTrue(os.path.exists(data_uri), 'File copied outside datadir')

        with open(random_file.name, 'rb') as f1, open(data_uri, 'rb') as f2:
            bytes_original = f1.read()
            bytes_copy = f2.read()
            self.assertEqual(bytes_original, bytes_copy,
                             'Copied contents do not match')

    def test_add_file(self):
        model = self.WorkspaceMockModel('test_inside', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        filename = os.path.join(workspace.data_uri, 'some_file.bin')
        url = 'file://{}'.format(filename)
        with open(filename, 'wb') as f:
            f.write(uuid.uuid4().bytes)
        workspace.add_file(url,
                           owner=self.user.username,
                           permissions=0o600)
        print(workspace.metadata())

    def test_add_file_outside(self):
        model = self.WorkspaceMockModel('test_outside', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        with self.assertRaises(ValueError):
            workspace.add_file('/dev/null',
                               owner=self.user.get_username(),
                               permissions=0o600)


    def test_extract_metadata(self):
        model = self.WorkspaceMockModel('test_extract_metadata', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        some_dir = os.path.join(workspace.datadir, 'dir1', 'dir2')
        dirname = os.makedirs(some_dir)
        some_file = tempfile.NamedTemporaryFile(dir=some_dir,
                                                suffix='.ext')
        filename = os.path.relpath(some_file.name, start=workspace.datadir)
        content = uuid.uuid4().bytes
        some_file.write(content)
        some_file.flush()
        some_file.seek(0)
        md5 = hashlib.new('md5')
        md5.update(content)

        fixed_id = 'fixed_id'
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=lambda: fixed_id) as handle_func:
            url, metadata = workspace.add_file(some_file,
                                               owner=self.user.username,
                                               permissions=0o600,
                                               filename=filename)

        REQUIRED_METADATA = {
            'id',
            'filename',
            'path',
            'suffix',
            'size',
            'hash',
            'date',
            'expiration_date',
            'owner',
            'url'
        }
        self.assertSetEqual(set(metadata.keys()), REQUIRED_METADATA,
                            'Missing required or unexpected automatic metadata')

        self.assertEqual(metadata['id'], fixed_id,
                         'Wrong file handle metadata')
        self.assertEqual(metadata['filename'],
                         os.path.split(some_file.name)[-1],
                         'Wrong filename metadata')
        self.assertEqual(metadata['path'],
                         os.path.relpath(some_dir, start=workspace.datadir),
                         'Wrong path metadata')
        self.assertEqual(metadata['suffix'],
                         os.path.splitext(some_file.name)[1][1:],
                         'Wrong file suffix metadata')
        self.assertEqual(metadata['size'],
                         os.stat(some_file.name).st_size,
                         'Wrong file size metadata')
        self.assertEqual(metadata['hash'], md5.hexdigest(),
                         'Wrong hash metadata')
        self.assertEqual(metadata['owner'], self.user.username,
                         'Wrong owner metadata')


@unittest.skipIf(not _user_is_root(), 'Local backend permission tests need root user')
@override_settings(**TEST_SETTINGS)
class LocalBackendTestsWithPermissions(LocalBackendTests):
    def setUp(self):
        super().setUp()
        self.backend = LocalBackend(basedir=self.tmpdir.name,
                                    enforce_permissions=True)

    def test_create_directories_permissions(self):
        model = self.WorkspaceMockModel('test_create_perms', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        self.assertEqual(os.stat(self.tmpdir.name).st_mode & 0o777, 0o755,
                        'Base backend directory does not have 755 permissions')
        datadir = os.path.join(self.tmpdir.name, 'data')
        self.assertEqual(os.stat(datadir).st_mode & 0o777, 0o755,
                        'Backend data directory does not have 755 permissions')
        metadatadir = os.path.join(self.tmpdir.name, 'metadata')
        self.assertEqual(os.stat(metadatadir).st_mode & 0o777, 0o755,
                        'Backend metadata directory does not have 755 permissions')

    def test_create_workspace_permissions(self):
        model = self.WorkspaceMockModel('test_create', self.user, {})
        workspace = self.backend.create_workspace(model=model)
        self.assertEqual(os.stat(workspace.uri).st_mode & 0o777, 0o700,
                         'Incorrect workspace directory permissions')


@unittest.skipIf(not _user_is_root(), 'Local backend tests need root user')
@override_settings(**TEST_SETTINGS)
class LocalMetadataBackendTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('tester',
                                             email='test@example.com')
        username = self.user.get_username()
        self.tmpdir = tempfile.TemporaryDirectory()
        os.chmod(self.tmpdir.name, 0o755)
        self.workspace = LocalWorkspace(backend=None,
                                        name='metadata-tests',
                                        owner=username,
                                        basedir=self.tmpdir.name,
                                        families={},
                                        initialize=True,
                                        enforce_permissions=True)

    def test_create(self):
        self.assertTrue(os.path.exists(self.workspace.metadatadir) and
                        os.path.isdir(self.workspace.metadatadir),
                        'Base data directory not created')
        self.assertEqual(os.stat(self.workspace.metadatadir).st_mode & 0o777,
                         0o700,
                         'Incorrect metadata data directory permissions')

    def test_create_family(self):
        family_url = self.workspace.create_family('family',
                                                  workspace=self.workspace,
                                                  owner=self.user.get_username(),
                                                  permissions=0o700)
        self.assertTrue(os.path.exists(family_url) and
                        os.path.isdir(family_url),
                        'Metadata family directory not created')
        self.assertEqual(os.stat(family_url).st_mode & 0o777,
                         0o700,
                         'Incorrect family directory permissions')

        owner = pwd.getpwuid(os.stat(family_url).st_uid).pw_name
        self.assertEqual(owner, self.user.get_username(),
                         'Incorrect family directory owner')

    def test_save_metadata(self):
        initial_meta = {
            'id': 'file_id',
            'str': 'str',
            'bool': True,
            'int': 1,
            'float': 1.23,
            'none': None,
            'list': [1,2,3],
            'dict': {'key': 'value'},
        }
        meta_file = self.workspace.save_metadata('file_id', 'family', initial_meta,
                                                 owner=self.user.get_username())
        with open(meta_file, 'r') as fd:
            saved_meta = json.load(fd)

        self.assertDictEqual(initial_meta, saved_meta,
                             'Saved metadata does not match original')

    def test_update_metadata(self):
        initial_meta = {
            'id': '1234',
            'modify': 'modify me',
            'unmodify': 'do not modify me'
        }
        self.workspace.save_metadata('file_id', 'family', initial_meta,
                                     owner=self.user.get_username())
        modified_meta = copy.deepcopy(initial_meta)
        modified_meta['modify'] = 'modified value'
        modified_meta['new'] = 'new value'

        meta_file = self.workspace.save_metadata('file_id', 'family', modified_meta,
                                                 owner=self.user.get_username())

        with open(meta_file, 'r') as fd:
            saved_meta = json.load(fd)

        expected_result = copy.deepcopy(initial_meta)
        expected_result.update(modified_meta)
        self.assertDictEqual(modified_meta, expected_result,
                             'Updated metadata is incorrect')


#@unittest.skipIf(not _user_is_root(), 'Local backend tests need root user')
@override_settings(**TEST_SETTINGS)
class LocalBackendCommitTests(TestCase):

    WorkspaceMockModel = collections.namedtuple('Workspace',
                                                ('name',
                                                 'owner',
                                                 'families'))

    def setUp(self):
        self.user = User.objects.create_user(username='tester',
                                             password='secret',
                                             email='test@example.com')
        self.tmpdir = tempfile.TemporaryDirectory()
        self.backend = LocalBackend(basedir=self.tmpdir.name,
                                    enforce_permissions=False)
        self.model = self.WorkspaceMockModel('test_commit', self.user,
                                             {'existing_family': 0,
                                              'cloudts': 0})
        self.workspace = self.backend.create_workspace(model=self.model)

    def _create_files(self, n=1, workspace=None, fixed_content=None):
        if workspace is None:
            workspace = self.workspace
        urls = []
        for i in range(n):
            with tempfile.NamedTemporaryFile() as f:
                filename = 'f{}.bin'.format(i)
                if fixed_content is None:
                    content = uuid.uuid4().bytes
                else:
                    content = fixed_content.encode(encoding='utf-8')
                f.write(content)
                f.flush()
                f.seek(0)
                url, metadata = workspace.add_file(f,
                                                   owner=self.user.username,
                                                   permissions=0o600,
                                                   filename=filename)
                urls.append(url)
                workspace.save_metadata(metadata['id'], 'cloudts', metadata,
                                        owner=self.user.username)
                workspace.save_metadata(metadata['id'], 'other',
                                        {'hello': 'world'},
                                        owner=self.user.username)
        return urls

    def test_commit(self):
        self._create_files(n=10)
        self.backend.commit(model=self.model)
        data_files = glob.glob('{}/*'.format(self.backend.datadir))
        metadata_files = glob.glob('{}/cloudts/*/*.json'
                                   .format(self.backend.metadatadir))

        self.assertListEqual([os.path.split(f)[-1] for f in sorted(data_files)],
                             [os.path.splitext(os.path.split(f)[-1])[0]
                              for f in sorted(metadata_files)])


    def test_double_commit_conflict(self):
        self._create_files(n=10)
        self.backend.commit(model=self.model)

        with self.assertRaises(CommitConflictException) as context:
            self.backend.commit(model=self.model)

        conflict_exception = context.exception
        self.assertSetEqual(set(conflict_exception.conflicts.keys()),
                            {'cloudts', 'other'})

    def test_new_workspace_metadata(self):
        self._create_files(10)
        self.backend.commit(model=self.model)

        new_model = self.WorkspaceMockModel('new_workspace', self.user,
                                             {'other': 1,
                                              'cloudts': 1})
        new_workspace = self.backend.create_workspace(model=new_model)
        new_metadata = new_workspace.metadata()
        for file_id in new_metadata:
            self.assertIn('other', new_metadata[file_id])
            self.assertDictContainsSubset({'hello': 'world'},
                                          new_metadata[file_id]['other'])


    def test_workspace_versions(self):
        num_versions = 10
        files_per_version = 3

        # Version 1
        self._create_files(n=3)
        self.backend.commit(model=self.model)

        # versions 2 to num_versions+1
        for i in range(num_versions):
            model = self.WorkspaceMockModel('workspace_{}'.format(i), self.user,
                                            families={'cloudts': i+1,
                                                      'other': i+1})
            workspace = self.backend.create_workspace(model=model)
            self._create_files(n=3, workspace=workspace)
            self.backend.commit(model=model)

        # Verify families and version numbers
        family_versions = self.backend.get_families()
        self.assertSetEqual(set(family_versions.keys()), {'cloudts', 'other'})

        for family, versions in family_versions.items():
            print(len(versions), num_versions)
            print(family, versions)
            self.assertListEqual(versions, list(range(1, num_versions+2)),
                                 'Unexpected number of versions for {}'
                                 .format(family))


    def test_commit_contents(self):
        workspace = self.workspace
        # First _create_files
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=100)) as handle_func:
            initial_urls = self._create_files(n=3)
        self.backend.commit(model=self.model)

        new_model = self.WorkspaceMockModel('new_workspace', self.user,
                                            {'other': 1,
                                             'cloudts': 1})
        new_workspace = self.backend.create_workspace(model=new_model)
        # Second _create_files
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=200)) as handle_func:
            new_urls = self._create_files(n=3, workspace=new_workspace)
        for url in new_urls:
            file_id = os.path.split(url)[-1]
            new_workspace.save_metadata(file_id, 'new_family',
                                        {'b': 456},
                                        owner=self.user.username)
        self.backend.commit(model=new_model)

        # Create a list of files that should be created by the local backend
        expected_files = set()
        # Files due to cloudts family on the first _create_files
        expected_files |= set(
            os.path.join(self.backend.metadatadir, 'cloudts', '1',
                         os.path.split(x)[-1] + '.json')
            for x in initial_urls)
        # Files due to other family on the first _create_files
        expected_files |= set(
            os.path.join(self.backend.metadatadir, 'other', '1',
                         os.path.split(x)[-1] + '.json')
            for x in initial_urls)
        # Files due to cloudts family on the second _create_files
        expected_files |= set(
            os.path.join(self.backend.metadatadir, 'cloudts', '2',
                         os.path.split(x)[-1] + '.json')
            for x in new_urls)
        # Files due to other family on the second _create_files
        expected_files |= set(
            os.path.join(self.backend.metadatadir, 'other', '2',
                         os.path.split(x)[-1] + '.json')
            for x in new_urls)
        # Files due to new_family family on the manual add_metadata
        expected_files |= set(
            os.path.join(self.backend.metadatadir, 'new_family', '1',
                         os.path.split(x)[-1] + '.json')
            for x in new_urls)

        created_files = set(x
                            for x in glob.glob(os.path.join(self.backend.metadatadir, '**'),
                                               recursive=True)
                            if os.path.isfile(x))
        self.assertSetEqual(expected_files, created_files,
                            'Unexpected files created during commit')

    def test_scan_simple(self):
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle()) as handle_func:
            self._create_files(3, fixed_content='content')
        self.workspace.scan()
        with open(self.workspace.tables, 'rb') as f:
            tables = pickle.load(f)
        self.assertTrue('cloudts' in tables, 'cloudts family not scanned')
        self.assertTrue('other' in tables, 'other family not scanned')
        self.assertSetEqual(set(tables['cloudts']['id']),
                            set(tables['other']['id']),
                            'Missing ids on both families')
        self.assertSetEqual(set(tables['cloudts'].columns),
                            set(('id', 'url', 'owner', 'filename', 'suffix',
                                 'path', 'size', 'hash',
                                 'date', 'expiration_date')),
                            'Missing metadata columns in cloudts family')
        self.assertSetEqual(set(tables['other'].columns),
                            set(('id', 'hello')),
                            'Missing metadata columns in other family')
        obj = FixedHandle()
        self.assertSetEqual(set(tables['cloudts']['id']),
                            set(obj() for _ in range(3)),
                            'Unexpected file ids in database')

    def test_scan_global(self):
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=100)) as handle_func:
            self._create_files(3, fixed_content='content')
        self.backend.commit(model=self.model)

        self.workspace.scan()
        with open(self.workspace.tables, 'rb') as f:
            v1_tables = pickle.load(f)

        # Second workspace using the first
        self.model = self.WorkspaceMockModel('new_workspace', self.user,
                                            {'cloudts': 1, 'other': 1})
        self.workspace = self.backend.create_workspace(model=self.model)

        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=200)) as handle_func:
            new_urls = self._create_files(3, fixed_content='new content')
        for i, url in enumerate(new_urls):
            file_id = os.path.split(url)[-1]
            self.workspace.save_metadata(file_id, 'new_family',
                                         {'b': i},
                                         owner=self.user.username)
            if i == 0:
                self.workspace.save_metadata(file_id, 'other',
                                             {'hello': 'goodbye'},
                                             owner=self.user.username)

        self.workspace.scan()
        with open(self.workspace.tables, 'rb') as f:
            v2_tables = pickle.load(f)

        self.assertTrue(all(original_id in v2_tables['cloudts']['id'].values
                            for original_id in v1_tables['cloudts']['id']),
                        'Missing ids of the committed workspace')
        # New ids (i.e >= 200) are not in the old tables.
        # Logic: (p implies q) is equivalent to: ((not p) or q)
        self.assertTrue(all(int(new_id) < 200 or
                            new_id not in v1_tables['cloudts']['id'].values
                            for new_id in v2_tables['cloudts']['id']),
                        'Unexpected ids in the committed workspace')
        self.assertTrue(all(v2_tables['other'].query('hello == "hello"')['id'] != '201'))
        self.assertTrue(all(v2_tables['other'].query('hello == "goodbye"')['id'] == '201'))


    def test_query(self):
        workspace = self.workspace
        # First _create_files
        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=100)) as handle_func:
            initial_urls = self._create_files(n=3)
        self.backend.commit(model=self.model)

        # Second workspace using the first
        self.model = self.WorkspaceMockModel('new_workspace', self.user,
                                            {'cloudts': 1, 'other': 1})
        self.workspace = self.backend.create_workspace(model=self.model)

        with patch.object(AbstractDataWorkspace, '_generate_handle',
                          side_effect=FixedHandle(start=200)) as handle_func:
            new_urls = self._create_files(3, fixed_content='new content')
        for i, url in enumerate(new_urls):
            file_id = os.path.split(url)[-1]
            self.workspace.save_metadata(file_id, 'new_family',
                                         {'b': i},
                                         owner=self.user.username)
            if i == 0:
                self.workspace.save_metadata(file_id, 'other',
                                             {'hello': 'goodbye'},
                                             owner=self.user.username)

        self.workspace.scan()
        sql = """\
        SELECT
            cloudts.id as id,
            other.hello as value
        FROM cloudts
        JOIN other
        WHERE cloudts.id == other.id
        """
        result = self.workspace.query(sql=sql)
        expected_result = pd.DataFrame(collections.OrderedDict([
            ('id', ['101', '102', '103', '201', '202', '203']),
            ('value', ['world', 'world', 'world', 'goodbye', 'world', 'world'])
        ]))
        self.assertTrue(pd.DataFrame.from_records(result).equals(expected_result),
                        'Unexpected query results')


class FixedHandle():
    def __init__(self, start=0):
        self.number = start

    def __call__(self):
        self.number += 1
        return str(self.number)
