# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


from django.contrib import admin

from .models import Workspace


admin.site.register(Workspace, admin.ModelAdmin)
