# [0.2.2] - 27/06/2017

* [RES-261] Fix incorrect behaviour of jobs when query gives empty result.
* [RES-262] User-specific configuration for target dataproc cluster name.
* [RES-265] Force NCVT to keep the database order and does not shuffle it for
  easier tracking of parallelizable jobs.

# [0.2.1]

* [RES-211] Separation of CloudTS interpreters in another repository.
* [RES-215] Support for different interpreter versions.
* [RES-229] Added NeuroRTTool support
* Documentation and script improvements thanks to a new
  administrator/developer/user: Jozef
* Rabbitmq with management support

# [0.2.0]

* Fixed several issues found while using the Data and Job API on real problems.
* Several fixes of [RES-214] and [RES-216], which are associated to failures due
  to REST API problems (connectivity, 50x errors, etc).
* Job stale/update verification to avoid recalculation using the date of
  modification of scripts, inputs and outputs (not parameters, these are not
  files).
* Basic conflict detection on postgres data API backend.
* Update of metadata definition of a workspace.
* Fixed error on wrong start id for workspaces.
* Faster job execution by not sending N job files but a single tar file that has
  all jobs.

# [0.1.0]

* First release with working job API

# [0.0.2rc2]

* Asynchronous tasks
* Fixes on the postgres backend

# [0.0.2rc1]

* Asynchronous tasks
* Postgres database backend for metadata

# [0.0.1-beta1] 14/08/2017

* First beta version of CloudTS.
* Development bump to setup versioning and start deploying.
* Functional Data API, but with many bugs as expected from a first version.
* Job API is an empty shell, does not really schedule anything.
* Local files Data API backend
* Google bucked Data API backend
* The metadata of the Data API backend is managed as a local filesystem with
  JSON files. This should be improved if we need to scale the web application
  server

# Template for changelog

## Changed/Added/Fixed/Removed

- list of elements that were changed, added, fixed or removed. One category per
  type (Changed, Added, Fixed, Removed)
