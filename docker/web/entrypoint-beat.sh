#!/bin/bash

set -e

echo "Waiting for RabbitMQ..."
until nc -z rabbitmq 5672
do
    sleep 1
done
echo "RabbitMQ is available"

celery beat \
       --app cloudts \
       --schedule /var/run/celerybeat-schedule.db \
       --pidfile /var/run/celerybeat.pid
