-- Creation of the default user
CREATE USER django_dbuser WITH PASSWORD 'django_dbpassword';
CREATE DATABASE django_db;
GRANT ALL PRIVILEGES ON DATABASE django_db TO django_dbuser;

-- Default timezone
SET TIME ZONE 'UTC';

-- -- Enable the hstore extension on the django_db database
-- \connect django_db;
-- CREATE EXTENSION IF NOT EXISTS hstore;

-- Creation of readonly user
CREATE ROLE readonly_user WITH LOGIN PASSWORD 'readonly_password' 
NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION VALID UNTIL 'infinity';

-- connect to the django_db to correctly configure its permissions
\connect django_db;
-- do not permit the public role to create tables
REVOKE CREATE ON SCHEMA public FROM PUBLIC;
-- do permit everything to the django user
GRANT ALL PRIVILEGES ON SCHEMA public TO django_dbuser;

-- TODO: after django creates its tables, change its permissions so that only
-- django_dbuser can read them. This is important for the auth table, for
-- example.
